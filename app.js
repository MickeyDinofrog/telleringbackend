var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const jwt = require('jsonwebtoken')
const bodyParser = require('body-parser');
const basicAuth = require('express-basic-auth');

require('./models/SocketIO');

const throttle = require('express-throttle-bandwidth')

var outhProvider = require('./routes/outh-provider');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var unionBank = require('./routes/unionbank-api');
var outsystemsRouter = require('./routes/outsystems');
var loansRouter = require('./routes/loans-application');
var login = require('./routes/login-routes');


var globalRouter = require('./routes/global-routes');

var app = express();

app.use(bodyParser.json());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');






//middleware test
const loggerx = (req, res, next) => {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type');
  
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', false);

  console.log("request is logged in here");
  next();
};




app.use(loggerx);
// app.use(basicAuth( { authorizer: myAuthorizer } ))
 
// function myAuthorizer(username, password) {
//     const userMatches = basicAuth.safeCompare(username, 'customuser')
//     const passwordMatches = basicAuth.safeCompare(password, 'custompassword')
    
//     return userMatches & passwordMatches
    
// }

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(throttle(1024 * 128)) // throttling bandwidth
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var sampleController = require('./routes/sample-controller');

var api_Version = 'v1';

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/outsystems', outsystemsRouter);
app.use('/loans-application', loansRouter);
app.use('/login-routes', login);
app.use('/global-routes', globalRouter);
app.use('/unionbank-api', unionBank);
app.use('/sample-controller', sampleController);
app.use('/oauth-provider', outhProvider);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
