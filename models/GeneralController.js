var sql = require('mssql');
var databases = require('./Database');
const { getPool } = require('./ConnectionPools')

module.exports = {

    getLoanFormsDetails: async function (branchcode, accountnumber) {
        const db_choice = databases.getDbBranches(branchcode); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

        let loanAccount = await pool.request().query(`exec _formsPrintingParam '${accountnumber}'`)

        console.log('luh ka diha', loanAccount.recordset[0])
        return loanAccount.recordset[0]

    },

    findLoan: async function(UserName) {
        const pool = await getPool('maindb', databases.maindb)
        const request = await pool.request()
          request.input('UserName', sql.VarChar, UserName)  
          return request.query(`SELECT * FROM _LoanApprovalHeader WHERE request_created_by = @UserName`).then((result)=>{  
          return result }) 
    },

    findApproveList: async function(FKLoanRole) {
        const pool = await getPool('maindb', databases.maindb)
        const request = await pool.request()
          request.input('FKLoanRole', sql.VarChar, FKLoanRole)  
          return request.query(`SELECT LH.id, LH.customer_name, LH.customerID, LH.account_number, LH.loan_product, LH.principal, LH.status FROM _LoanApprovalHeader AS LH
LEFT JOIN _LoanApprovalRequestApprovers AS LA ON LH.id = LA.fk_header_id WHERE LA.role = @FKLoanRole`).then((result)=>{  
          return result }) 
    },

    pendingRequestCount: async function(UserName) {
        const pool = await getPool('maindb', databases.maindb)
        const request = await pool.request()
          request.input('UserName', sql.VarChar, UserName)  
          return request.query(`SELECT count(*) as PendingRequest FROM _LoanApprovalHeader WHERE status = 'PENDING' AND request_created_by = @UserName`).then((result)=>{  
          return result }) 
    },

    pendingCountApprover: async function(FKLoanRole) {
        const pool = await getPool('maindb', databases.maindb)
        const request = await pool.request()
          request.input('FKLoanRole', sql.VarChar, FKLoanRole)  
          return request.query(`SELECT count(*) as PendingRequest FROM _LoanApprovalHeader AS LH
LEFT JOIN _LoanApprovalRequestApprovers AS LA ON LH.id = LA.fk_header_id WHERE LH.status = 'PENDING' AND LA.role = @FKLoanRole`).then((result)=>{  
          return result }) 
    },

    insertHeader: async function(header_object,UserName, attachment_object){

        
          const db_choice = databases.getDbBranches('cen'); 
          const pool = await getPool(db_choice.database_alias, db_choice.database_name)

          const transaction = new sql.Transaction(pool)

          return transaction.begin().then(async (err)=>{
            let is_done_all = true;

            const request = new sql.Request(transaction)
            .input('UserName', sql.VarChar, UserName) 
            .input('fullname', sql.VarChar, header_object.fullname)  
            .input('fkcustomeridloan', sql.VarChar, header_object.fkcustomeridloan)    
            .input('accountnumber', sql.VarChar, header_object.accountnumber)    
            .input('loanproductname', sql.VarChar, header_object.loanproductname)
            .input('createdby', sql.VarChar, header_object.createdby)
            .input('verifiedby', sql.VarChar, header_object.verifiedby)
            .input('principal', sql.Int, header_object.principal)
            .input('numberofterm', sql.Int, header_object.numberofterm)
            .input('date_approved', sql.DateTime, new Date())
            /*.input('date_expired', sql.DateTime, (new Date(Date.now()+2*24*60*60*1000)))*/

            is_done_all =  await (
                request.query(`INSERT INTO _LoanApprovalHeader 
                    (customer_name, customerid, account_number, loan_product, loan_created_by, loan_verified_by, principal, term, status, approved_by, date_approved, date_applied, request_created_by) 
                    VALUES 
                    (@fullname, @fkcustomeridloan, @accountnumber, @loanproductname, @createdby, @verifiedby, @principal, @numberofterm, 'PENDING', '', @date_approved, '3/23/2020', @username);  SELECT SCOPE_IDENTITY() AS latest_id;`)
                  .then(async (result)=>{

                        for(const valx of attachment_object)
                        {
                            
                            var res = valx.fileType.split("/");
                            var file_name = valx.fileName + '.' +res[1]
                            const requestx = new sql.Request(transaction)
                            .input('latest_id', sql.Int, result.recordset[0].latest_id) 
                            .input('fileName', sql.VarChar, file_name) 
                            .input('dataType', sql.VarChar, valx.dataType) 
                            .input('fileType', sql.VarChar, valx.fileType) 
                            .input('date_added', sql.DateTime, new Date())
                            await requestx.query(`INSERT INTO _LoanApprovalAttachments 
                            (fk_approval_id, file_name, file_type, attachment_type, date_added) 
                            VALUES 
                            (@latest_id, @fileName, @fileType, @dataType, @date_added);`).then((result)=>{

                            }).catch(e=>{
                                console.log("nag errowr", e)
                                return false;
                            }) 
                        }


                        //for automatic 
                        const requesty = new sql.Request(transaction)
                        let the_included_settings = await(requesty.query(`select * from _LoanApprovalApproversSettings las
                                                                        where ` + header_object.principal + ` >= las.min and ` + header_object.principal + ` <= las.max`)
                                                          .then(result=>{
                                                              return result.recordset
                                                          }).catch(e=>{
                                                              console.log("nag error si ako", e)
                                                              return false;
                                                          })
                                                    )

                        if(the_included_settings===false)
                                  return false;
                                  
                            for(const valx of the_included_settings)
                                {
                                    const requestz = new sql.Request(transaction)  
                                    await (requestz.query(`INSERT INTO [dbo].[_LoanApprovalRequestApprovers]
                                    ([fk_header_id]
                                    ,[role]
                                    ,[status]
                                    ,[date_transacted]
                                    ,[remarks])
                              VALUES
                                    (`+ result.recordset[0].latest_id +`
                                    ,'`+ valx.role +`'
                                    ,'PENDING'
                                    ,GETDATE()
                                    ,'')`).then(result=>{


                                        
                                    }).catch(e=>{
                                        console.log("basin diri", e)
                                        return false;
                                    }))


                                }
                        

                    
                        return result.recordset[0].latest_id;  
                  }).catch(e=>{
                      console.log("error ang header", e)
                      return false;
                  })
            );

            if(is_done_all!==false)
            return transaction.commit().then((err)=>{
                console.log("committed the crime")
                return is_done_all
            })
            else
            return transaction.rollback().then((err)=>{
                return "error"
               // break;
            })
            
          }).catch((err)=>{
            return "error during transaction"
        });

          
                  
    },

    insertFiles: async function(latest_id,files_upload,file_type){
          const pool = await getPool('maindb', databases.maindb)
          for(const valx of files_upload)
          { 
              var res = valx.fileType.split("/");
              var file_name = valx.fileName + '.' +res[1]
              const request = await pool.request()
              .input('latest_id', sql.Int, latest_id) 
              .input('fileName', sql.VarChar, file_name) 
              .input('dataType', sql.VarChar, valx.dataType) 
              .input('fileType', sql.VarChar, valx.fileType) 
              .input('date_added', sql.DateTime, new Date())
              await request.query(`INSERT INTO _LoanApprovalAttachments 
              (fk_approval_id, file_name, file_type, attachment_type, date_added) 
              VALUES 
              (@latest_id, @fileName, @fileType, @dataType, @date_added);`).then((result)=>{
              }) 
          }  
    },
}