
var sql = require('mssql');
var databases = require('./Database');
const { getPool } = require('./ConnectionPools')

var _ = require('lodash');
const { request } = require('../app');
const { padStart } = require('lodash');


module.exports = {

    computeESPLoan: async function(customerid) {
        
        // strSQL = "SELECT  TOP 1 AccountNumber, AccountStatus, Balance "
        //     strSQL &= "FROM dbo.SavingsAccount "
        //     strSQL &= "WHERE FKSAProductIDAccount = '39' " 'Equity Savings Product
        //     strSQL &= "AND FKCustomerIDAccount='" & pCID & "' "

        //EXECUTE _LoanGetESPEvaluation '<savingsaccountno>'

        try {
            const db_choice = databases.getDbBranches('001'); 
            const pool = await getPool(db_choice.database_alias, db_choice.database_name);

            let dst = [];
            
            await (pool.request().query(`SELECT  TOP 1 AccountNumber, AccountStatus, Balance
            FROM dbo.SavingsAccount WHERE FKSAProductIDAccount = '39'
            AND FKCustomerIDAccount='` + customerid + `'
            `).then((result)=>{
                    let res = result.recordset;
                if(res.length>=1)
                {
                    //return res;
                    return pool.request().query(`EXECUTE _LoanGetESPEvaluation '` + res[0].AccountNumber + `'`).then((result)=>{
                        let res2 = result.recordset;
                            
                        if(res2.length>=1)
                        {   dst.push(res2);
                            return pool.request().query(`SELECT AccountNumber, FKCustomerIDAccount, 
                            LastName + ', ' + FirstName + ' ' + MiddleName AS AccountName, SAProductName, 
                            Balance, HoldBalance, AccountStatus FROM dbo.SavingsAccount INNER JOIN dbo.Customer 
                            ON dbo.Customer.CustomerID = dbo.SavingsAccount.FKCustomerIDAccount INNER JOIN dbo.SavingsProduct 
                            ON dbo.SavingsProduct.SAProductID = dbo.SavingsAccount.FKSAProductIDAccount 
                            WHERE AccountNumber = '` + res[0].AccountNumber + `'`).then((result)=>{
                                let res3 = result.recordset;
                                
                                if(res3.length>=1)
                                {
                                    dst.push(res3);
                               
                                    return pool.request().query(`SELECT ISNULL(SUM(Balance),0) AS ShareBal FROM dbo.ShareAccount
                                        WHERE FKCustomerIDAccount = '` + customerid + `'`).then(result=>{
                                            let res4 = result.recordset;
                                            if(res4.length>=1)
                                            {
                                                dst.push(res4);

                                                return pool.request().query(`EXECUTE _CustomerDelinqValidate '` + customerid +`'`).then(result=>{
                                                    let res5 = result.recordset;
                                                    
                                                    if(res5.length>=1)
                                                    {
                                                        dst.push(res5);
                                                        return pool.request().query(`SELECT ISNULL(SUM(Balance),0) AS SavingsBal FROM dbo.SavingsAccount 
                                                        WHERE FKCustomerIDAccount = '`+ customerid +`' AND FKSAProductIDAccount = '01' 
                                                        `).then(result=>{
                                                            res6 = result.recordset;
                                                            if(res6.length>=1)
                                                            {
                                                                dst.push(res6);
                                                                
                                                                return pool.request().query(`SELECT COUNT(*) AS DEPOSIT_COUNT 
                                                                FROM 
                                                                ( 
                                                                SELECT MonthDep, YearDep, COUNT(*) AS CNT 
                                                                FROM 
                                                                ( 
                                                                SELECT 
                                                                TransactionDate, 
                                                                Amount, 
                                                                BalanceLocator, 
                                                                DATEPART(MONTH, TransactionDate) AS 'MonthDep', 
                                                                DATEPART(YEAR, TransactionDate) AS 'YearDep' 
                                                                FROM dbo.SATransaction 
                                                                INNER JOIN dbo.SavingsAccount 
                                                                ON dbo.SavingsAccount.AccountNumber = dbo.SATransaction.FKAccountNumberTransaction 
                                                                INNER JOIN dbo.SATransactionType 
                                                                ON dbo.SATransactionType.SATransactionCode = dbo.SATransaction.FKTransactionTypeCode 
                                                                WHERE FKAccountNumberTransaction = '`+ res[0].AccountNumber + `' 
                                                                AND BalanceLocator = 1 
                                                                AND IsCancelled = 0 
                                                                )ASDAS 
                                                                GROUP BY MonthDep, YearDep 
                                                                )SADAS `).then(result=>{
                                                                    let res7 = result.recordset;

                                                                       if(res7.length>=1)
                                                                       {
                                                                            dst.push(res7);

                                                                            return pool.request().query(`SELECT  ISNULL(SUM(PrincipalBalance),0) AS ESPLoanBal 
                                                                            FROM dbo.LoanAccount 
                                                                            WHERE LEFT(AccountStatus,3) IN ('CUR', 'PAS') 
                                                                            AND PrincipalBalance > 0 
                                                                            AND FKLoanProductIDAcct = '90' 
                                                                            AND FKCustomerIDLoan = '` + customerid + `' `).then(result=>{
                                                                                let res8 = result.recordset;

                                                                                if(res8.length>=1)
                                                                                {
                                                                                    dst.push(res8);

                                                                                    return dst;


                                                                                } else return;
                                                                            }).catch(error=>{
                                                                                return {
                                                                                    status: 'failed',
                                                                                    message: error.message,
                                                                                }
                                                                            });
                                                                       }   
                                                                       else return;

                                                                }).catch(error=>{
                                                                    return {
                                                                        status: 'failedx',
                                                                        message: error.message
                                                                    }
                                                                });

                                                            }
                                                            else return;
                                                        }).catch(error=>{
                                                            return {
                                                                status: 'failedq',
                                                                message: error.message
                                                            }
                                                        });
                                                    }
                                                    else return;

                                                }).catch(error=>{

                                                });
                                            }
                                            else return;
                                        }).catch((error)=>{
                                        return {
                                            status: 'failedc',
                                            message: error.message
                                        }
                                    });
                                }
                                else
                                return;

                            }).catch(error=>{
                                return {
                                    status: 'failedxx',
                                    message: error.message
                                }
                            });
                        }

                        else
                        {
                            return;
                        }

                       
                    }).catch(
                        error=>{
                            return {
                                status: 'failed',
                                message: error.message
                            }
                        }
                    )

                    
                }

                else
                {
                    return;
                }


            }).catch((error)=>{
                return {
                    status: 'failed',
                    message: error.message
                };
            }));
            


            //start dynamic calculation;
            
            let espParams = dst[0];
            let espBalance = dst[6];
            let depCount = dst[5];
            let loanableAmount = espParams[0].LOANABLE_AMOUNT;

            let netLoanableAmount = Number(loanableAmount) - Number(espBalance[0].ESPLoanBal);
            
            let finalDepCount = depCount[0].DEPOSIT_COUNT;
            let status = 'QUALIFIED';
            let score = 0;

            if(Number(finalDepCount)>=12)
                score++;
            else score--;
            



            return {
                LoanableAmount: loanableAmount,
                NetLoanableAmount: netLoanableAmount,
                ESPLoanBalance: espBalance[0].ESPLoanBal,
                InterestRate: espParams[0].INT_RATE,
                ESPStatus: 'QUALIFIED',
            };

            



        } catch (error) {
            return {
                status: 'failed',
                message: error.message
            };
        }


    }
}