var sql = require('mssql');
var databases = require('./Database');
const { getPool } = require('./ConnectionPools')

//find by id
var onerow = 'sdd'


async function jamming(request, value, method, table_name) {
    
 //  console.log("i am thy request", request)
 request.input('method', sql.VarChar, method)
    return await request.query(`INSERT INTO [_TellersUpdateLogs]
                     ([table_name]
                     ,[remarks]
                     ,[table_row_id]
                     ,[fk_user_id]
                     ,[date_updated]
                     )
                 VALUES
                     ('`+ table_name +`'
                     ,@method
                     ,`+ value.id +`
                     ,'`+ value.filed_by +`'
                     ,GETDATE()
                 )`).then(res=>{
         console.log("res is me", res)
         return {
             status: true,
             message: ''
         };
     }).catch(err=>{
         console.log("failed logs", err)
         return {
             status: false,
             message: err
         };
     })
 }
 

module.exports = {
    findById: async function(id){
        
      return sql.connect(databases.maindb).then((err)=>{
           var request = new sql.Request()
           return request.query("select top 1 * from systemuser where username='"+ id +"'").then((result)=>{
  
               return result; 
           })
       })
       
    },


    getShortageOverageIndex: async function(branchcode){

        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

        return pool.request().query(`select fkbranchid, fk_date_id, branchname, teller_date from _TellerShortageOverageReport sor
        inner join systemuser s on sor.created_by = s.username
        inner join _tellerrunningdate trd on sor.fk_date_id = trd.id
        inner join branch on s.fkbranchid = branch.branchid
        where s.FKBranchID = '` + branchcode + `'
        group by fkbranchid, fk_date_id, branchname, teller_date order by teller_date desc
        `).then((result)=>{

            return result; 
        }).catch(err=>{
            console.log("atay ra",err)
            return err;
        })
    },

    saveDeclaration: async function(object, userObj){
        const db_choice = databases.getDbBranches('cen')
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

        //const transaction = new sql.Transaction(pool)

        // await transaction.begin().catch(err=>{
        //     return {
        //         status: 'failed',
        //         message: err.message
        //     }
        // })

        const request = new sql.Request(pool)
        request.input('shortage', sql.Decimal(10, 2), object.shortage)
        request.input('overage', sql.Decimal(10, 2), object.overage)
        request.input('dateid', sql.Int, userObj.fk_date_id)
        request.input('username', sql.VarChar, object.username)
        request.input('remarks', sql.VarChar, object.remarks)
        request.input('createdby', sql.VarChar, userObj.username)
        return request.query(`INSERT INTO [dbo].[_TellerShortageOverageReport]
                            ([shortage]
                            ,[overage]
                            ,[fk_date_id]
                            ,[fk_teller_id]
                            ,[is_notified]
                            ,[is_accepted]
                            ,[bkpr_remarks]
                            ,[created_by]
                            ,[row_version])
                    VALUES
                            (@shortage
                            ,@overage
                            ,@dateid
                            ,@username
                            ,0
                            ,0
                            ,@remarks
                            ,@createdby
                            ,GETDATE())`).then(resx=>{
                                console.log('naunsa diay diri')
                                return {
                                    status: 'success',
                                    message: '',
                                }
                            }).catch(err=>{
                                console.log('naunsa diay', err)
                                return {
                                    status: 'failed',
                                    message: err.message
                                }
                            })
        
    },

    save: async function(object, fk_date_id, created_by) {

            const db_choice = databases.getDbBranches('cen'); 
            const pool = await getPool(db_choice.database_alias, db_choice.database_name)

            const transaction = new sql.Transaction(pool)

            return transaction.begin().then(async (err)=>{
                
                console.log("transaction start", err)
                
                                let arrlen = object.length;
                                let is_done_all =  {
                                    status: true,
                                    message: ''
                                };
                                console.log("my length is " + arrlen)
                                for(const value of object)
                                {
                                    const request = new sql.Request(transaction)
                                    request.input('shortage', sql.Decimal, value.shortage)
                                    request.input('overage', sql.Decimal, value.overage)
                                    request.input('dateid', sql.Int, fk_date_id)
                                    request.input('username', sql.VarChar, value.username)
                                    request.input('remarks', sql.VarChar, value.remarks)
                                    request.input('createdby', sql.VarChar, created_by)
                                  if(!is_done_all.status) break;
                                  is_done_all =  await (
                                        request.query(`INSERT INTO [dbo].[_TellerShortageOverageReport]
                                                                ([shortage]
                                                                ,[overage]
                                                                ,[fk_date_id]
                                                                ,[fk_teller_id]
                                                                ,[is_notified]
                                                                ,[is_accepted]
                                                                ,[bkpr_remarks]
                                                                ,[created_by]
                                                                ,[row_version])
                                                        VALUES
                                                                (@shortage
                                                                ,@overage
                                                                ,@dateid
                                                                ,@username
                                                                ,0
                                                                ,0
                                                                ,@remarks
                                                                ,@createdby
                                                                ,GETDATE())`).then((result)=>{
                                            
                                            console.log("insertion start ", result)
                                            return {
                                                status: true,
                                                message: '',
                                            };                                
                                        }).catch(err=>{
                                            console.log("rollback's a hell bitch.");
                                            return {
                                                status: false,
                                                message: err,
                                            };  
                                           
                                        })
                                    );
                  
                                }
                                if(is_done_all.status)
                                return transaction.commit().then((err)=>{
                                    return {
                                        status: 'success',
                                        message: '',
                                    }
                                })
                                else
                                return transaction.rollback().then((err)=>{
                                    return {
                                        status: 'failed',
                                        message: is_done_all,
                                    }
                         
                                })
                
                               
            }).catch((err)=>{
                return {
                    status: 'success',
                    message: err,
                }
            });

        


    },


    updateShortageOverage: async function(object, method){
        try {

            const db_choice = databases.getDbBranches('cen'); 
            const pool = await getPool(db_choice.database_alias, db_choice.database_name)

            const transaction = new sql.Transaction(pool)

            return transaction.begin().then(async (err)=>{

                let is_done_all = {
                    status: true,
                    message: ''
                };

                const request = new sql.Request(transaction)
                request.input('remarks', sql.VarChar, object.remarks)
                console.log("i came here.")
                
                if(typeof object.id == 'undefined') {
                    console.log('no id')

                    const toUpdate = await request.query(`select top 1 id from _TellerShortageOverageReport
                                                            where fk_teller_id='${object.username}' and fk_date_id=${object.date_id}`)
                    
                    object.id = toUpdate.recordset[0].id
                    
                }

                
                //remarks lang muna update natin ngayon sir.
                is_done_all = await(request.query(`
                UPDATE [dbo].[_TellerShortageOverageReport]
                SET 
                [bkpr_remarks] = @remarks
              WHERE id = ` + object.id).then(async (result)=>{

                    const requestx = new sql.Request(transaction)
                   return await jamming(requestx, object, method, '_TellerShortageOverageReport');
                
                }).catch(err=>{
                    console.log("error is mex ", err)
                    return {
                        status: false,
                        message: err
                    };
                })
            
            );


                if(is_done_all.status)
                return transaction.commit().then((err)=>{
                    console.log("committed the crime")
                    return {
                        status: 'success',
                        message: '',
                    }
                })
                else
                 {
                    transaction.rollback();
                    return {
                        status: 'failed',
                        message: is_done_all.message.originalError.info.message,
                    };
                 }


            })
            
        } catch (error) {
            return {
                status: 'failed',
                message: error
            }
        }

    },

}