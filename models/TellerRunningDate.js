var sql = require('mssql');
var databases = require('./Database');
var _ = require('lodash');
const { getPool } = require('./ConnectionPools')
var moment = require('moment');



module.exports = {
    save: async function(object){

        sql.connect(databases.maindb).then((err)=>{
            //log for start of connection
            console.log("connection start", err)
            const transaction = new sql.Transaction()
            transaction.begin().then(async (err)=>{
                //log for start of transaction
                console.log("transaction start", err)

                let arrlen = object.length;
      
                console.log("my length is " + arrlen)
                for(const value of object)
                {
                    const request = new sql.Request(transaction)

                    await (
                        request.query(`
                        INSERT INTO [dbo].[_TellerRunningDate]
                        ([teller_date])
                        VALUES
                        ('`+ value +`')
                        `).then((result)=>{
                            
                            console.log("insertion start ", result)

                        }).catch(err=>{

                            transaction.rollback().then((err)=>{
                                console.log("rollback's a hell bitch.");
                            })
                        })
                    );
  
                }

                transaction.commit().then((err)=>{
                    console.log("transaction is committed", err)
                })


            });

       });

       return "tabachuychuy";
    },

    /**
     * 
     * @param {*} date_string WALA ni nagamit kay maka back date man diay
     * @returns the last 1 day of tellerrunning date equivalent to central server current date
     */
    findAll: async function(date_string, hasWhere = true) {

        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)
        
        whereMessage = ` where teller_date ='${date_string}' `

        date_string = moment(date_string).format('YYYY-MM-DD');

        return pool.request().query(`select id as value, convert(varchar, teller_date, 101) as label from _tellerrunningdate
        ${hasWhere ? whereMessage : ' '}
        order by id desc
        `).then((result)=>{
            return result; 
        })
    },

    findOneDate: async function(date_id) {

        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

        return pool.request().query(`select top 1 id as value, convert(varchar, teller_date, 101) as label from _tellerrunningdate
        where id = ` + date_id + `
        `).then((result)=>{

            return result; 
        })
    }
}