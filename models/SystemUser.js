

var sql = require('mssql');
var databases = require('./Database');
const { getPool } = require('./ConnectionPools')
var moment = require('moment');
var _ = require('lodash');
const Big = require('big.js');
const { compileClientWithDependenciesTracked } = require('jade');


/**
This function will get the Branch's current transaction date. it will return an object
i.e: {currenttransactiondate: '5/20/2020'}
**/
async function getCurrentBranchDate(branchid) 
{
    const db_cen = databases.getDbBranches(branchid);
    const pool_cen = await getPool(db_cen.database_alias, db_cen.database_name);

    return pool_cen.request().query(`select top 1 currenttransactiondate from institutionparameter`).then(
        resultx=>{
          return resultx.recordset[0];
        });
}

/**
This function will get the Branch's current transaction date AGAINST THE CENTRALIZED teller running date. it will return an object
i.e: {currenttransactiondate: '5/20/2020'}
**/
async function getCurrentBranchDateForDropDown(branchid) 
{ 

    const db_cen = databases.getDbBranches(branchid);
    const pool_cen = await getPool(db_cen.database_alias, db_cen.database_name);


    return pool_cen.request().query(`select top 1 currenttransactiondate from institutionparameter`).then(
       async resultx=>{
         // return resultx.recordset[0];
          const db_cenx = databases.getDbBranches('cen');
          const pool_cenx = await getPool(db_cenx.database_alias, db_cenx.database_name);
        
          return pool_cenx.request().query(`select id as value, convert(varchar, teller_date, 101) as label from _tellerrunningdate where teller_date='` + moment(resultx.recordset[0].currenttransactiondate).format('YYYY-MM-DD') + `'`).then(resxx=>{
            return resxx.recordset;
          });

        });
}


async function getBanks()
{
    const db_cen = databases.getDbBranches('cen');
    const pool_cen = await getPool(db_cen.database_alias, db_cen.database_name);

    return pool_cen.request().query(`select id as value, bank_name as label from _tellerbanks`).then(
        resultx=>{
          return resultx.recordset;
        });
}

module.exports = {

    findAllByBranchToFillGrid: async function(branchcode) {

        const pool = await getPool('maindb', databases.maindb)
        return pool.request().query("select * from systemuser where FKBranchID='"+ branchcode +"'").then((result)=>{
            
                         return result; 
                     })
    },

    findAllActiveTellers: async function(branchcode) {
        
        const db_choice = databases.getDbBranches(branchcode);
        
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)


        return pool.request().query(`select username, teller, shortage, overage, remarks, is_approved, cashonhand from (
                    select username, lastname + ', ' + firstname + ' ' + middlename as teller, 0 as shortage, 0 as overage, '' as remarks, 1 as is_approved, cashonhand

                    from systemuser where fkbranchid='` + branchcode + `'
                    and fkroleid='TELL' and userstatus=1
                    )sfdd
                    where UserName IN (SELECT teller_username FROM [_tellerIsActiveDate] where date_entry = (SELECT top 1 CurrentTransactionDate FROM InstitutionParameter))
            `).then(async (result)=>{

            let retval = [];
                

              let systemCurrentDate = await pool.request().query(`select top 1 currenttransactiondate from institutionparameter`).then(
                  resultx=>{
                    return resultx.recordset[0];
                  });
              
                  
              /*
               *    GET THE DECLARED TELLERS ON THE SAME DATE IN CENTRAL DB
               */
                  let declaredCentralTellers = [];

                  const db_cent = databases.getDbBranches('cen')
                  const cent_pool = await getPool(db_cent.database_alias, db_cent.database_name)

                  declaredCentralTellers = await cent_pool.request().query(`select fk_teller_id, shortage, overage, bkpr_remarks as remarks from _TellerShortageOverageReport inner join _TellerRunningDate
                  on _TellerRunningDate.id = _TellerShortageOverageReport.fk_date_id
                  inner join _TellerMatchUsers on _TellerMatchUsers.branch_username = _TellerShortageOverageReport.fk_teller_id
                  inner join SystemUser on _TellerMatchUsers.central_username = SystemUser.UserName
                  where teller_date='${moment(systemCurrentDate.currenttransactiondate).format('MM/DD/YYYY')}' and FKBranchID='${branchcode}'`)

                  

                
              /*
               *    END OF GETTING THE DECLARED TELLERS ON THE SAME DATE IN CENTRAL DB
               */

                for(const row of result.recordset)
                {

                    let qryResult = [];
                    
                  try {
                    
                        qryResult = await pool.request().query(`exec _GETSHORTAGEOVERRAGE '`+ row.username + `'`).then(
                            resultx=>{

                               return resultx;
                               
                            // return result.recordset[0].NETCASH;
                            }
                        ).catch(e=>{
                            console.log("errorx", e)
                            return e;
                        });
                  } catch (error) {
                      
                      console.log("error me", error)
                      return error;
                      break;
                  }

                  try {
                    if(qryResult.recordset[0])
                    { 
  
                        
                        if(qryResult.recordset[0].NETCASH!=null)
                        {   
                            let netcash = new Big(qryResult.recordset[0].NETCASH)
                            let difference = netcash.minus(row.cashonhand)
                          //  let difference = Number(qryResult.recordset[0].NETCASH) - Number(row.cashonhand);
  
                            
  
                            if(difference==0)
                              {
                                  if(Number(qryResult.recordset[0].NETCASH)<0)
                                      row.overage = netcash.times(-1)
                                  else row.shortage = netcash;
  
                              }
  
                          else
                          {
                              if(difference<0)
                                row.shortage = difference * -1;
                            else if(difference>0)
                                row.overage = difference;
                          }
  
  
                        }
  
                        else
                          row.overage = row.cashonhand
                    }
                  } catch (error) {
                      row.shortage = 0
                      row.overage = 0
                  }


                  let hasDeclared = declaredCentralTellers.recordset.filter(val => val.fk_teller_id == row.username)

                  row.has_declared = Boolean(hasDeclared.length)
                  row.remarks = hasDeclared.length ? hasDeclared[0].remarks : ''

                   retval.push(row);
                }
            
                
            


            return {
                tellers: retval,
                currentDate: systemCurrentDate
            }; 
        }).catch(e=>{
            console.log("naunsa", e)
            return e;
        })
    },

    findAllActiveTellersTransaction: async function(branchcode, date_id) {

        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

              //  const pool = await getPool('maindb', databases.maindb)
                return pool.request().query(`select sor.id, shortage, overage, 1 as is_approved, fk_teller_id as username, (tm.lastname + ', ' + tm.firstname + ' ' + tm.middlename)teller, bkpr_remarks as remarks from _tellerShortageOverageReport sor
                inner join systemuser u on sor.created_by = u.username
                inner join (select * from _TellerMatchUsers inner join systemuser su on central_username = username
                    where fkbranchid='` + branchcode + `')tm on sor.fk_teller_id = tm.branch_username
                where fk_date_id=` + date_id + ` and u.fkbranchid='` + branchcode + `'    
                `).then((result)=>{
                   
                    return result; 
                })
            },

    //effective only for quasar dropdown
    findAllActiveTellersForDropdown: async function(branchcode, date_id) {


        const db_choice = databases.getDbBranches(branchcode); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)
        let retval = [];

        return pool.request().query(`select username as value, lastname + ', ' + firstname + ' ' + middlename as label from systemuser where fkbranchid='` + branchcode + `'
        and fkroleid='TELL' and  UserName IN (SELECT FKUserNameStatus FROM TellerStatus WHERE SignedStatus = 1)`).then(async (result)=>{
            
            if(result.recordset.length)
            retval.push(result.recordset)
            
            let systemCurrentDate = '';

            if(date_id!=='none')
            {    
                const db_cen = databases.getDbBranches('cen');
                const pool_cen = await getPool(db_cen.database_alias, db_cen.database_name);

                systemCurrentDate = await pool_cen.request().query(`select top 1 teller_date as currenttransactiondate from _tellerrunningdate where id = ` + date_id).then(
                    resultx=>{
                      return resultx.recordset[0];
                    });

                    retval.push(systemCurrentDate);

                return retval;
            }

            else
            {
               return getCurrentBranchDate(branchcode).then(res=>{
                    console.log('kabalik ko', res);
                    systemCurrentDate = res;
                    retval.push(systemCurrentDate);
                    return retval;
                }).catch(err=>{
                    console.log('ohoyy', err.message);
                    return err.message;
                });

                
            }

            
            
        })   

    
       // return retval;
    },


    initializeCoci: async function(branchcode)
     {
         let retval = {};

        return getCurrentBranchDateForDropDown(branchcode).then(res=>{
            retval.systemCurrentDate = res;

           return getBanks().then(resx=>{
                retval.banks = resx;
                
                return retval;

            }).catch(errx=>{
                console.log('ohoyyx', err);
                return errx;
            });

         }).catch(err=>{
             console.log('ohoyy', err);
             return err;
         });

     },

     initializeCociIndex: async function(selectiontype, branchcode, currentuser)
     {
        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name);
 
        
        if(selectiontype=='ADMIN' || selectiontype=='SOHEAD')
        {
            return pool.request().query(`select tch.id, b.branchname, processed_by, teller_date from _tellerCOCIDetails tcd
            inner join _tellerCOCIHeader tch on tcd.fk_header_id = tch.id
            inner join _tellerrunningdate trd on trd.id = tch.fk_date_id
            inner join systemuser su on tch.processed_by = su.username
            inner join branch b on b.branchid = su.fkbranchid
            where tch.status='ACTIVE' and su.fkbranchid='` + branchcode + `'
            group by tch.id, b.branchname, processed_by, teller_date`).then(res=>{
                        return res.recordset;
                });
        }

        else
        {
            return pool.request().query(`select tch.id, b.branchname, processed_by, teller_date from _tellerCOCIDetails tcd
            inner join _tellerCOCIHeader tch on tcd.fk_header_id = tch.id
            inner join _tellerrunningdate trd on trd.id = tch.fk_date_id
            inner join systemuser su on tch.processed_by = su.username
            inner join branch b on b.branchid = su.fkbranchid
            where tch.status='ACTIVE' and su.fkbranchid='` + branchcode + `'
            and processed_by='` + currentuser + `'
            group by tch.id, b.branchname, processed_by, teller_date`).then(res=>{
                        return res.recordset;
                });
        }
     },

     loadCociDetails: async function(coci_id)
     {
        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name);

            return pool.request().query(`select tcd.id, tch.fk_date_id, convert(varchar, trd.teller_date, 101) as fk_date_label,
            payee_name, check_no, amount, description, convert(varchar, tcd.date_processed, 101) as date_deposit, accountnumber,
            (select top 1 bank_name from _TellerBanks where id = tcd.fk_bank_id)fk_bank_label,
            (select top 1 bank_name from _TellerBanks where id = tcd.fk_bank_deposit_id)fk_bank_deposit_label
            from _tellerCOCIDetails tcd
            inner join _tellerCOCIHeader tch on tcd.fk_header_id = tch.id
            inner join _tellerrunningdate trd on trd.id = tch.fk_date_id
            inner join systemuser su on tch.processed_by = su.username
            inner join branch b on b.branchid = su.fkbranchid
            where tcd.fk_header_id = ` + coci_id).then(res=>{
                        return res.recordset;
                });
        
     },

     loadTellerBranches: async function()
     {
        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name);

            return pool.request().query(`select branchid as value, branchname as label
                        from branch where branchstatus=1`).then(res=>{
                        var dbres = res.recordset
                        dbres.push({
                            value: 'ALL',
                            label: 'ALL'
                        });

                        return dbres
                });
     },


     getIdOfTelleringDateString: async function(dateString)
     {
        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name);

            return pool.request().query(`select top 1 id as date_id from _tellerrunningdate where teller_date='` + dateString + `'`).then(res=>{
                        return res.recordset[0];

                }).catch(err=>{
                    return {
                        status:'failed',
                        message: err.message
                    }
                });
     },


     getIdOfCociHeader: async function(username, dateString)
     {
        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name);

            return pool.request().query(`select top 1 tch.id as date_id from _TellerCOCIHeader tch
            inner join _TellerRunningDate trd on tch.fk_date_id = trd.id
            where processed_by='` + username + `' and teller_date='` + dateString + `'`).then(res=>{
                        return res.recordset[0];

                }).catch(err=>{
                    return {
                        status:'failed',
                        message: err.message
                    }
                });
     },


     getAllTellersAndBranches: async function()
     {
        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name);

            return pool.request().query(`select username as value, (lastname + ', ' + firstname + ' ' + middlename)label, fkbranchid, branchname  from systemuser su
                                    inner join branch b on su.fkbranchid = b.branchid
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '001' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '002' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '003' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '023' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '004' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '005' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '006' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '007' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '008' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '009' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '010' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '011' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '012' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '013' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '014' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '015' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '016' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '017' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '018' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '019' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '020' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '021' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '022' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '023' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '024' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '025' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, '026' as fkbranchid, 'none' as branchname
                                    union all
                                    SELECT 'ALL' as value, 'ALL' as label, 'ALL' as fkbranchid, 'ALL BRANCHES' as branchname
                                    `).then(res=>{
                                            return res.recordset;

                                    }).catch(err=>{
                                        return {
                                            status:'failed',
                                            message: err.message
                                        }
                                    });
     },




     saveCOCIReport: async function(coci_object)
     {
        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

        const transaction = new sql.Transaction(pool)

        return transaction.begin().then(async (err)=>{
            
                        let is_done_all = {
                            status: true,
                            message: ''
                        };
            const request = new sql.Request(transaction)

           return request.query(`INSERT INTO [dbo].[_TellerCOCIHeader]
            ([fk_date_id]
            ,[processed_by]
            ,[date_processed]
            ,[status])
            VALUES
            (` + coci_object.header.fk_date_id + `
            ,'` + coci_object.header.processed_by + `'
            ,getdate()
            ,'` + coci_object.header.status + `'); SELECT SCOPE_IDENTITY() AS latest_id;`).then(async res=>{
                

                if(res.recordset[0].latest_id !== null)
                {
                    for(const value of coci_object.details)
                    {
                      if(!is_done_all.status)
                      break;
                      const requestx = new sql.Request(transaction)
                      requestx.input('headerid', sql.Int, res.recordset[0].latest_id)
                      requestx.input('datedeposit', sql.DateTime, value.date_deposit)
                      requestx.input('payeename', sql.VarChar, value.payee_name)
                      requestx.input('checkno', sql.VarChar, value.check_no)
                      requestx.input('bankid', sql.Int, value.fk_bank_id)
                      requestx.input('amount', sql.Decimal, value.amount)
                      requestx.input('description', sql.VarChar, value.description)
                      requestx.input('accountnumber', sql.VarChar, value.accountnumber)
                      requestx.input('depositid', sql.Int, value.fk_bank_deposit_id)
                      is_done_all = await requestx.query(`INSERT INTO [dbo].[_TellerCOCIDetails]
                      ([fk_header_id]
                      ,[date_processed]
                      ,[payee_name]
                      ,[check_no]
                      ,[fk_bank_id]
                      ,[amount]
                      ,[description]
                      ,[accountnumber]
                      ,[fk_bank_deposit_id])
                VALUES
                      (@headerid
                      ,@datedeposit
                      ,@payeename
                      ,@checkno
                      ,@bankid
                      ,@amount
                      ,@description
                      ,@accountnumber
                      ,@depositid)`).then(response=>{

                        if(!response.rowsAffected[0]>=1)
                            return {
                                status: false,
                                message: 'did not saved anything from this detail'
                            };

                            return {
                                status: true,
                                message: ''
                            };


                      }).catch(err=>{
                          return {
                              status: false,
                              message: err.message
                          };
                      })
    
    
                    }


                    
                    is_done_all.status ? transaction.commit() : transaction.rollback();
                    return is_done_all;

                }

                else
                {
                    transaction.rollback();
                    return {
                        status: false,
                        message: 'Header not saved',
                    }
                }

               

            });

        }).catch(err=>{
            console.log("aguy error ko bai", err);
            transaction.rollback();
            return {
                status: false,
                message: err.message
            }
        });

     },




    /*
    loan approval begins here
    */
    findAllPendingLoans: async function(branchcode){

        const db_choice = databases.getDbBranches(branchcode); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

        return pool.request().query(`select top 100 (lastname + ', ' + firstname + ' ' + middlename)fullname, la.fkcustomeridloan, loanproductname,
        accountnumber, principal, numberofterm, la.createdby, la.verifiedby
        from loanaccount la
        inner join customer c on la.fkcustomeridloan = c.customerid
        inner join loanproduct p on la.fkloanproductidacct = p.loanproductid
        where left(accountstatus, 3)='VER'`).then((result)=>{
            

            return result; 
        })
    },


    findAttachments: async function(req_id, attachment_type='VOUCHER'){
        const pool = await getPool('maindb', databases.maindb)
        return pool.request().query(`select id, fk_approval_id, attachment_type, file_name, file_type, 
convert(varchar, date_added, 101) as date_added
from _LoanApprovalAttachments
        where fk_approval_id=` + req_id).then((result)=>{
            return result; 
        })
    },



    findRespectiveApprovers: async function(req_id){
        const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

        return pool.request().query(`select * from [_LoanApprovalRequestApprovers]
         where fk_header_id=` + req_id).then((result)=>{
                    return result; 
                })
    },


    findHeaderById: async function(req_id){
        const pool = await getPool('maindb', databases.maindb)
        return pool.request().query(`select * from _LoanApprovalHeader where id=`+req_id).then((result)=>{
            

            return result; 
        })
    },

    approveRequest: async function(object){
        try {
            const db_choice = databases.getDbBranches('cen'); 
            const pool = await getPool(db_choice.database_alias, db_choice.database_name)
            const transaction = new sql.Transaction(pool)
            return transaction.begin().then(async (err)=>{
    
                  let is_done_all = true;
                    const request = new sql.Request(transaction)
                    const randomstring = Math.random().toString(36).substr(2, 5);
                    is_done_all =  await (
                          
               
                         request.query(`UPDATE _LoanApprovalRequestApprovers SET
                         status='APPROVED', date_transacted=GETDATE()
                         WHERE fk_header_id=` + object.id + ` and role='` + object.user_position + `'
                        `).then(async (result)=>{

                            let totalPending  = 5;

                            totalPending = await(request.query(`select count(*)remaining from _LoanApprovalRequestApprovers
                                                        where status='PENDING' AND fk_header_id=` + object.id).then((result)=>{
                                                            console.log("hi there boii", result.recordset[0].remaining)
                                                            return result.recordset[0].remaining;
                                                            // return result; 
                                                 })
                                          );
                            

                            if(totalPending==0)
                            {
                                return await(
                                    request.query(`UPDATE _LoanApprovalHeader SET status='APPROVED', approval_code='`+ randomstring +`'
                                    where id=` + object.id + ` and account_number='` + object.account_number + `' and status not in ('APPROVED', 'DRAFT', 'REJECTED', 'CANCELLED')`).then(async (result)=>{
                                                  
                                      
                                      console.log("hoy naupdate na", result.rowsAffected[0])
                                      if(result.rowsAffected[0]>=1)
                                          return {
                                                   status: 'APPROVED',
                                                   approval_code: randomstring
                                                 };
                                      else return "error: Already Updated or no account found."
                                      //return "saved"
                                    }).catch(err=>{
                                        console.log("rollback's a hell bitch.", err);
                                        return false;
                                       
                                    })
                                )
                            }

                            else
                            {
                                return {
                                  status: 'approved_partial',
                                  message: 'none so far' 
                                };
                            }

                        })

                          
                      );
    
                  
                  if(is_done_all!==false)
                  return transaction.commit().then((err)=>{
                      console.log("committed the crime")
                      return is_done_all
                  })
                  else
                  return transaction.rollback().then((err)=>{
                      return "error"
                     // break;
                  })
  
                 
              }).catch((err)=>{
                  return "error during transaction"
              });

            
        } catch (error) {
            return error
        }
    },


    //start of alo
    updateRequestStatus: async function(object){
        try {
            const pool = await getPool('maindb', databases.maindb)
            const transaction = new sql.Transaction(pool)
            return transaction.begin().then(async (err)=>{
    
                  let is_done_all = true;
                    const request = new sql.Request(transaction)

                    is_done_all =  await (

                        request.query(`UPDATE _LoanApprovalRequestApprovers SET
                        status='` + object.status + `', date_transacted=GETDATE(),
                        remarks='` + object.approver_remarks + `'
                        WHERE fk_header_id=` + object.id + ` and role='` + object.user_position + `'
                       `).then(async (result)=>{

                              return  request.query(`UPDATE _loanapprovalheader SET status='` + object.status + `', approver_remarks='`+ object.approver_remarks +`'
                                where id=` + object.id + ` and account_number='` + object.account_number + `'
                                and status not in ('APPROVED')`).then(async (result)=>{

                                if(result.rowsAffected[0]>=1)
                                    return {
                                            new_status: object.status,
                                            //approval_code: randomstring
                                        };
                                else return "error: Already Updated or no account found."
                                //return "saved"
                            }).catch(err=>{
                                console.log("rollback's a hell bitch.", err);
                                return false;
                                
                            })

                       })

                          
                      );
    
                  
                  if(is_done_all!==false)
                  return transaction.commit().then((err)=>{
                      console.log("committed the crime")
                      return is_done_all
                  })
                  else
                  return transaction.rollback().then((err)=>{
                      return "error"
                     // break;
                  })
  
                 
              }).catch((err)=>{
                  return "error during transaction"
              });

            
        } catch (error) {
            return error
        }
    },
    //end of alo

    saveForApproval: async function(header_object, attachment_object, id_object){
       
        try {
            console.log("i went here 1")
            const pool = await getPool('maindb', databases.maindb)
                
                //start the hell
                const transaction = new sql.Transaction(pool)
                return transaction.begin().then(async (err)=>{
        
                      let is_done_all = true;
    
                      console.log("i went here 2")
                        const request = new sql.Request(transaction)
                        console.log("money ko", header_object.principal);
                        console.log("i went here 3")
                        is_done_all =  await (

                              request.query(`INSERT INTO [dbo].[_LoanApprovalHeader]
                                                ([customer_name]
                                                ,[customerid]
                                                ,[account_number]
                                                ,[loan_product]
                                                ,[loan_created_by]
                                                ,[loan_verified_by]
                                                ,[principal]
                                                ,[term]
                                                ,[status]
                                                ,[approved_by]
                                                ,[date_approved]
                                                ,[date_applied]
                                                ,[request_created_by])
                                            VALUES
                                                ('` + header_object.fullname + `'
                                                ,'` + header_object.fkcustomeridloan + `'
                                                ,'` + header_object.accountnumber + `'
                                                ,'` + header_object.loanproductname + `'
                                                ,'` + header_object.createdby + `'
                                                ,'` + header_object.verifiedby + `'
                                                ,'` + header_object.principal + `'
                                                ,'` + header_object.numberofterm + `'
                                                ,'PENDING'
                                                ,''
                                                ,'1/1/1900'
                                                ,'3/23/2020'
                                                ,'` + header_object.request_created_by + `'
                                                ); SELECT SCOPE_IDENTITY() AS latest_id;`).then(async (result)=>{
                                                
                                                for(const valx of attachment_object)
                                                {   
                                                    const requestx = new sql.Request(transaction)
                                                    await (requestx.query(`INSERT INTO [dbo].[_LoanApprovalAttachments]
                                                    ([fk_approval_id]
                                                    ,[file_name]
                                                    ,[file_type]
                                                    ,[date_added]
                                                    ,[attachment_type])
                                            VALUES
                                                    (` + result.recordset[0].latest_id + `
                                                    ,'` + valx.file_name +`'
                                                    ,'` + valx.file_type + `'
                                                    ,'3/23/2020'
                                                    ,'VOUCHER')`).then(result=>{


                                                        
                                                    }))

                                                }


                                                for(const valx of id_object)
                                                {   
                                                    const requestxx = new sql.Request(transaction)
                                                    await (requestxx.query(`INSERT INTO [dbo].[_LoanApprovalAttachments]
                                                    ([fk_approval_id]
                                                    ,[file_name]
                                                    ,[file_type]
                                                    ,[date_added]
                                                    ,[attachment_type])
                                            VALUES
                                                    (` + result.recordset[0].latest_id + `
                                                    ,'` + valx.file_name +`'
                                                    ,'` + valx.file_type + `'
                                                    ,'3/23/2020'
                                                    ,'ID')`).then(result=>{


                                                        
                                                    }))

                                                }



                                                
                                  console.log("insertion start ", result)
                                  return result.recordset[0].latest_id;                              
                              }).catch(err=>{
                                  console.log("rollback's a hell bitch.", err);
                                  return false;
                                 
                              })
                          );
        
                      
                      if(is_done_all!==false)
                      return transaction.commit().then((err)=>{
                          console.log("committed the crime")
                          return is_done_all
                      })
                      else
                      return transaction.rollback().then((err)=>{
                          return "error"
                         // break;
                      })
      
                     
                  }).catch((err)=>{
                      return "error during transaction"
                  });
                //end the hell
    
            
        } catch (error) {
            console.log("tirada", error)            
        }

    },


    removeFiles: async function(object) {

        try {
            const pool = await getPool('maindb', databases.maindb)
            
            const request = new sql.Request(pool)
            
            return request.query(`DELETE FROM _loanapprovalattachments WHERE
                          fk_approval_id=` + object.id + ` and file_name='` + object.file_name + `'
                    `).then(result=>{
                        console.log("pag delete ako ang result", result)
                        return "success"
            }).catch(err=>{
                return {
                   status: 'error from request',
                   message: err
                }
            })


        } catch (error) {
            return {
                status: "error from all",
                message: error
            }
        }
    },

    //to update request, update files
    updateFilesUpload: async function(header_object, attachment_object, id_object){
        
         try {

             const pool = await getPool('maindb', databases.maindb)
                 
                 //start the hell
                 const transaction = new sql.Transaction(pool)
                 return transaction.begin().then(async (err)=>{
         
                       let is_done_all = true;
     

                         const request = new sql.Request(transaction)
                            console.log("naa ko oh", header_object.id)
                         is_done_all =  await (

                               request.query(`UPDATE _loanapprovalheader SET status='PENDING'
                                    where id=` + header_object.id + ` and account_number='` + header_object.account_number + `'
                                    and status not in ('APPROVED')`).then(async (result)=>{
                                                 
                                                 for(const valx of attachment_object)
                                                 {   
                                                     const requestx = new sql.Request(transaction)
                                                     await (requestx.query(`INSERT INTO [dbo].[_LoanApprovalAttachments]
                                                     ([fk_approval_id]
                                                     ,[file_name]
                                                     ,[file_type]
                                                     ,[date_added]
                                                     ,[attachment_type])
                                             VALUES
                                                     (` + header_object.id + `
                                                     ,'` + valx.file_name +`'
                                                     ,'` + valx.file_type + `'
                                                     ,'3/23/2020'
                                                     ,'VOUCHER')`).then(result=>{
 
 
                                                         
                                                     }))
 
                                                 }
 
 
                                                 for(const valx of id_object)
                                                 {   
                                                     const requestxx = new sql.Request(transaction)
                                                     await (requestxx.query(`INSERT INTO [dbo].[_LoanApprovalAttachments]
                                                     ([fk_approval_id]
                                                     ,[file_name]
                                                     ,[file_type]
                                                     ,[date_added]
                                                     ,[attachment_type])
                                             VALUES
                                                     (` + header_object.id + `
                                                     ,'` + valx.file_name +`'
                                                     ,'` + valx.file_type + `'
                                                     ,'3/23/2020'
                                                     ,'ID')`).then(result=>{
 
 
                                                         
                                                     }))
 
                                                 }
 
 
 
                                                 
                                   console.log("insertion start ", result)
                                   return "saved";                              
                               }).catch(err=>{
                                   console.log("rollback's a hell bitch.", err);
                                   return false;
                                  
                               })
                           );
         
                       
                       if(is_done_all!==false)
                       return transaction.commit().then((err)=>{
                           console.log("committed the crime")
                           return is_done_all
                       })
                       else
                       return transaction.rollback().then((err)=>{
                           return "error"
                          // break;
                       })
       
                      
                   }).catch((err)=>{
                       console.log("error daw oh", err)
                       return "error during transaction"
                   });
                 //end the hell
     
             
         } catch (error) {
             console.log("tirada", error)            
         }
 
     },

     

     getIndexes: async function (username, role)
     {

        try {

            let qry = `select id, customerid, customer_name, loan_product, account_number, principal, term, status from _loanapprovalheader
            where request_created_by='`+ username +`'`+ (role=='SOHEAD' ? ` OR status='PENDING'` : ``);

            const pool = await getPool('maindb', databases.maindb)
            
            const request = new sql.Request(pool)
            
            return request.query(qry).then(result=>{
                            return result
            }).catch(err=>{
                return {
                   status: 'error from request',
                   message: err
                }
            })


        } catch (error) {
            return {
                status: "error from all",
                message: error
            }
        }

     }


}