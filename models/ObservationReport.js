var sql = require('mssql');
var databases = require('./Database');
const { getPool } = require('./ConnectionPools')

async function jamming(request, value, method, table_name) {
   
//  console.log("i am thy request", request)
   return await request.query(`INSERT INTO [_TellersUpdateLogs]
                    ([table_name]
                    ,[remarks]
                    ,[table_row_id]
                    ,[fk_user_id]
                    ,[date_updated]
                    )
                VALUES
                    ('`+ table_name +`'
                    ,'`+ method +`'
                    ,`+ value.id +`
                    ,'`+ value.filed_by +`'
                    ,GETDATE()
                )`).then(res=>{
        console.log("res is me", res)
        return {
            status: true,
            message: ''
        };
    }).catch(err=>{
        console.log("failed logs", err)
        return {
            status: false,
            message: err
        };
    })
}


module.exports = {

    findAllTransactionType: async function(){

        return [
            {
                value: 'NO-OBSERVATION',
                label: '(NO)No Observation',
                description: '(NO)No Observation',
            },
            {
                value: 'Wrong amount in words',
                label: 'Wrong amount in words',
                description: 'Wrong amount in words',
            },
            {
                value: 'Wrong amount in Figures',
                label: 'Wrong amount in Figures',
                description: 'Wrong amount in Figures',
            },
            {
                value: 'Wrong account number',
                label: 'Wrong account number',
                description: 'Wrong account number',
            },
            {
                value: 'No signature of the member',
                label: 'No signature of the member',
                description: 'No signature of the member',
            },
            {
                value: 'Wrong account validation',
                label: 'Wrong account validation',
                description: 'Wrong account validation',
            },
            {
                value: 'Incorrect Date',
                label: 'Incorrect Date',
                description: 'Incorrect Date',
            },
            {
                value: 'No countersigned of alterations/erasures',
                label: 'No countersigned of alterations/erasures',
                description: 'No countersigned of alterations/eraures',
            },
            {
                value: 'No deposit slip filed',
                label: 'No deposit slip filed',
                description: 'No deposit slip filed',
            },
            {
                value: 'No account name',
                label: 'No account name',
                description: 'No account name',
            },
            {
                value: 'No signature of checker by (LRV)',
                label: 'No signature of checker by (LRV)',
                description: 'No signature of checker by (LRV)',
            },
            {
                value: 'No stamp/incorrect stamp',
                label: 'No stamp/incorrect stamp',
                description: 'No stamp/incorrect stamp',
            },
            {
                value: 'No initial of the Teller',
                label: 'No initial of the Teller',
                description: 'No initial of the Teller',
            },
            {
                value: 'Not verified by CA',
                label: 'Not verified by CA',
                description: 'Not verified by CA',
            },
            {
                value: 'Others',
                label: 'Others',
                description: 'Others',
            },
        ];

     /* return sql.connect(databases.maindb).then((err)=>{
           var request = new sql.Request()
           return request.query(`select * from (
            select 'SA-' + SATransactionCode as value, '(SA)'+Description as label, 'SA-' + SATransactionCode as description from satransactiontype
            union all
            select 'LOAN-'+LNTransactionCode as value, '(LN)'+Description as label, 'LOAN-'+LNTransactionCode as description from LoanTransactionType
            union all
            select 'TD-'+TDTransactionCode as value, '(TD)'+Description as label, 'TD-'+TDTransactionCode as description from TDTransactionType
            union all
            select 'SHRE-'+SCTransactionCode as value, '(SH)'+Description as label, 'SHRE-'+SCTransactionCode as description from ShareTransactionType
            union all
            select 'MISC-'+MiscTransactionCode as value, '(MISC)'+MiscDescription as label, 'MISC-'+MiscTransactionCode as description from MiscTrnType
            union all
            select 'NO-OBSERVATION' as value, '(NO)No Observation' as label, 'NO-OBSERVATION' as description
            )ffs order by value
           `).then((result)=>{  
                console.log('i am the resx ', result);
               return result; 
           })
       }) */


       
    },


    checkBackend: async function(){
        
       // console.log(jamming())
        const pool = await getPool('maindb', databases.maindb)
        const transaction = new sql.Transaction(pool)

        return transaction.begin().then(async (res)=>{

            var request = new sql.Request(transaction)
            
            await jamming(request);
            
            console.log("i am the thevar wow")
           
            transaction.commit().then((err)=>{

            })
        });

    },


    updateObservation: async function(object, method){
        try {
            const pool = await getPool('maindb', databases.maindb)
            const transaction = new sql.Transaction(pool)

            return transaction.begin().then(async (err)=>{

               
                let is_done_all = {
                    status: true,
                    message: ''
                };

                let theDate = new Date(object.date_regularized + " UTC");

                const request = new sql.Request(transaction)
                request.input('dateid', sql.Int, object.fk_date_id)
                request.input('tellerid', sql.VarChar, object.fk_teller_id)
                request.input('transactioncode', sql.VarChar, object.transaction_code)
                request.input('description', sql.VarChar, object.description)
                request.input('date_regularized', sql.DateTime, theDate)
                request.input('is_regularized', sql.VarChar, object.is_regularized)

                console.log("i came here.")
                is_done_all = await(request.query(`
                UPDATE [_TellersObservationReport]
                SET [fk_date_id] = @dateid
                   ,[fk_teller_id] = @tellerid
                   ,[transaction_code] = @transactioncode
                   ,[description] = @description
                   ,[date_regularized] = @date_regularized
                   ,[additional_remarks] = ''
                   ,[is_regularized] = @is_regularized
                 
                  
              WHERE id= `+ object.id + `
                `).then(async (result)=>{

                    const requestx = new sql.Request(transaction)
                   return await jamming(requestx, object, method, '_TellersObservationReport');
                


                }).catch(err=>{
                    console.log("error is mex ", err)
                    return {
                        status: false,
                        message: err
                    };
                })
            
            );


                if(is_done_all.status)
                return transaction.commit().then((err)=>{
                    console.log("committed the crime")
                    return {
                        status: 'success',
                        message: '',
                    }
                })
                else
                 {
                    transaction.rollback();
                    return {
                        status: 'failed',
                        message: is_done_all.message.originalError.info.message,
                    };
                 }


            })
            
        } catch (error) {
            return {
                status: 'failed',
                message: error
            }
        }

    },

    getAllDataForUpdate: async function(date_id, branch_code){
      
        const db_choice = databases.getDbBranches('cen');
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

        return pool.request().query(`select tor.id, fk_date_id, convert(varchar, teller_date, 101) as fk_date_label, fk_teller_id, (s.lastname + ', ' + s.firstname + ' ' + s.middlename)teller_name, transaction_code, transaction_code as transaction_label, tor.description, is_regularized, convert(varchar, date_regularized, 101)date_regularized, filed_by, 1 as is_old  from _tellersobservationreport tor inner join
        _tellerrunningdate trd on tor.fk_date_id = trd.id
        inner join _TellerMatchUsers tmu on tor.fk_teller_id = tmu.branch_username
        inner join systemuser s on tmu.central_username = s.username
        inner join systemuser ss on tor.filed_by = ss.UserName
        where fk_date_id=` + date_id + ` and s.fkbranchid='` + branch_code + `' and ss.fkbranchid='${branch_code}'`).then((result)=>{
            
            console.log('by the way ', result);
            return result; 
        })

    },


    getAllTellersForDefault: async function(date_id, date_label, branch_code, filed_by){
        
        const db_choice = databases.getDbBranches(branch_code);
        
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)

          let tellers = await pool.request().query(`select (lastname + ', ' + firstname)label, username as value from systemuser where fkroleid='TELL'
          and  UserName IN (SELECT teller_username FROM [_tellerIsActiveDate] where date_entry = '${date_label}')`)

          return pool.request().query(`select '' as description, '`+ filed_by +`' as filed_by, ` + date_id + ` as fk_date_id, '` + date_label + `' as fk_date_label,
          username as fk_teller_id, (lastname + ', ' + firstname + ' ' + middlename)teller_name, 0 as is_regularized,
          'NO-OBSERVATION' as transaction_code, '(NO)No Observation' as transaction_label, 1 as is_old          
		  from (
		  		  select * from systemuser where fkroleid='TELL' and userstatus=1
		  ) s 
          where fkbranchid='` + branch_code + `' and fkroleid='TELL' and  UserName IN (SELECT teller_username FROM [_tellerIsActiveDate] where date_entry = '${date_label}')`).then((result)=>{
  
              return {
                  result: result.recordset,
                  tellers: tellers.recordset
              }; 
          })
  
      },

    save: async function(object){
        
              const pool = await getPool('maindb', databases.maindb)
              const transaction = new sql.Transaction(pool)
              return transaction.begin().then(async (err)=>{
                   
        
                        let arrlen = object.length;
                        let is_done_all = {
                            status: true,
                            message: ''
                        };
                        
                        const checkRequest = new sql.Request(transaction)
                        let existingObservation = await checkRequest.query(`select count(*) as theCount from _TellersObservationReport where fk_date_id=68`)

                        console.log('the count is ', existingObservation)

                        for(const value of object)
                        {              
                            const request = new sql.Request(transaction)
                            request.input('dateid', sql.Int, value.fk_date_id)
                            request.input('tellerid', sql.VarChar, value.fk_teller_id)
                            request.input('transactioncode', sql.VarChar, value.transaction_code)
                            request.input('description', sql.VarChar, value.description)
                            request.input('filedby', sql.VarChar, value.filed_by)
                            request.input('isRegularized', sql.Bit, value.transaction_code=='NO-OBSERVATION')
                          if(!is_done_all.status) break;
                          is_done_all =  await (
                                

                                request.query(`INSERT INTO [dbo].[_TellersObservationReport]
                                                        ([fk_date_id]
                                                        ,[fk_teller_id]
                                                        ,[transaction_code]
                                                        ,[description]
                                                        ,[date_acknowledged]
                                                        ,[date_regularized]
                                                        ,[additional_remarks]
                                                        ,[is_regularized]
                                                        ,[filed_by]
                                                        ,[row_version])
                                                VALUES
                                                        (@dateid
                                                        ,@tellerid
                                                        ,@transactioncode
                                                        ,@description
                                                        ,GETDATE()
                                                        ,'1/1/1900'
                                                        ,''
                                                        ,@isRegularized
                                                        ,@filedby
                                                        ,GETDATE())`).then((result)=>{
                                    
                                    console.log("insertion start ", result)
                                    return {
                                        status: true,
                                        message: '',
                                    };                              
                                }).catch(err=>{
                                    console.log("rollback's a hell bitch.", err.message);
                                    return {
                                        status: false,
                                        message: err
                                    };
                                   
                                })
                            );
          
                        }
                        if(is_done_all.status)
                        return transaction.commit().then((err)=>{
                            return {
                                status: 'succeeded',
                                message: '',
                            }
                        })
                        else
                        return transaction.rollback().then((err)=>{
                            return {
                                status: 'failed',
                                message: is_done_all,
                            }
                    
                        })
        
                       
                    }).catch((err)=>{
                        return {
                            status: 'failed',
                            message: err
                        }
                    });
        
           
        
         
            },

    getObservationIndex: async function(branchcode){

        return sql.connect(databases.maindb).then((err)=>{
            var request = new sql.Request()
            return request.query(`select fkbranchid, fk_date_id, branchname, teller_date from _tellersobservationreport tor
            inner join _tellerrunningdate trd on tor.fk_date_id = trd.id
            inner join systemuser s on tor.filed_by = s.username
            inner join branch on s.fkbranchid = branch.branchid
            where fkbranchid='` + branchcode + `'
            group by fkbranchid, fk_date_id, branchname, teller_date`).then((result)=>{
                return result; 
            })
        })
    }
}