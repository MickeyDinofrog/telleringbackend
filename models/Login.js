var sql = require('mssql');
var databases = require('./Database');
const { getPool } = require('./ConnectionPools')
var moment = require('moment')
module.exports = {

    findUser: async function(UserName, Password) {
         const db_choice = databases.getDbBranches('cen'); 
        const pool = await getPool(db_choice.database_alias, db_choice.database_name)
        const request = await pool.request()
          request.input('UserName', sql.VarChar, UserName)  
          request.input('Password', sql.VarChar, Password)  
          return request.query(`SELECT * FROM SystemUser WHERE FKRoleID<>'TELL' and  UserName = @UserName COLLATE SQL_Latin1_General_CP1_CS_AS and Password = @Password COLLATE SQL_Latin1_General_CP1_CS_AS`).then((result)=>{  
          return result }) 
    },

    changePassword: async function (object) {
      const db_choice = databases.getDbBranches('cen');
      const pool = await getPool(db_choice.database_alias, db_choice.database_name)

      const request = await pool.request()
      request.input('username', sql.VarChar, object.username)
      request.input('newpassword', sql.VarChar, object.newpassword)
      request.input('oldpassword', sql.VarChar, object.oldpassword)

      return request.query(`UPDATE SystemUser set Password = @newpassword where UserName= @username and Password = @oldpassword `).then(res=>{
        console.log(res)
        return res.rowsAffected;
      })

    },

    findToken: async function(UserName, Password) {

    /*  if(branch=='001')
         const pool = await getPool('maindb', databases.maindb)
      else if(branch=='002')
        const pool = await getPool('nabun', databases.nabun)
*/
        const pool = await getPool('maindb', databases.maindb)
        const request = await pool.request()
          request.input('UserName', sql.VarChar, UserName)  
          return request.query(`SELECT * FROM _fis_access_tokens WHERE username = @UserName`).then((result)=>{  
          return result }) 
    },

    insertToken: async function(UserName, accessToken){
          const pool = await getPool('maindb', databases.maindb)
          const request = await pool.request()
            .input('UserName', sql.VarChar, UserName)  
            .input('accessToken', sql.VarChar, accessToken)    
            .input('date_issued', sql.DateTime, new Date())
            .input('date_expired', sql.DateTime, (new Date(Date.now()+2*24*60*60*1000)))
            return request.query(`INSERT INTO _fis_access_tokens (username, api_token, date_issued, date_expire) VALUES (@UserName, @accessToken, @date_issued, @date_expired)`).then((result)=>{
            return result; })
    },

    updateToken: async function(UserName, accessToken){
          const pool = await getPool('maindb', databases.maindb)
          const request = await pool.request()
            .input('UserName', sql.VarChar, UserName)  
            .input('accessToken', sql.VarChar, accessToken)    
            .input('date_expired', sql.DateTime, (new Date(Date.now()+2*24*60*60*1000)))
            return request.query(`UPDATE _fis_access_tokens SET date_expire = @date_expired, api_token = @accessToken WHERE username = @UserName`).then((result)=>{
            return result; })
    },



    loginUser: async function (branch, username, password) {
      console.log('branch is', branch)
      const db_choice = databases.getDbBranches(branch); 
      const pool = await getPool(db_choice.database_alias, db_choice.database_name)
      const request = await pool.request()
        request.input('UserName', sql.VarChar, username)  
        request.input('Password', sql.VarChar, password)  
        return request.query(`SELECT * FROM SystemUser WHERE UserName = @UserName COLLATE SQL_Latin1_General_CP1_CS_AS and Password = @Password COLLATE SQL_Latin1_General_CP1_CS_AS`).then((result)=>{  
        return result }) 
    },


    //for tshirt
    getMigsSearch: async function (branch, name, year) {
      const db_choice = databases.getDbBranches('tshirt');
      const pool = await getPool(db_choice.database_alias, db_choice.database_name)
      const request = await pool.request()
        request.input('name', sql.VarChar, name.substr(0, 2) == '00' ? name : name + '%')  
        request.input('branchcode', sql.VarChar, branch)
        

        let dataList = await request.query(`select CustomerID, CustomerName, mn.claim_status as claimstatus, cl.claim_status, date_transacted, cl.claim_type, representative_name, tshirt_size, transacted_by from (select top 10 * from migs_nonmigs
          where ${name.substr(0, 2) == '00' ? 'CustomerID = @name ' : 'CUSTOMERNAME LIKE @name ' } and branchcode=@branchcode  and MIGSTAT='MIGS' and migs_year=${year} order by CUSTOMERNAME) mn
                  left join claim_logs cl on mn.CustomerID = cl.fk_customer_id  and mn.migs_year = cl.batch_year`)

          let counter = 0;

          const reducer = (accumulator, currentValue, index, array)=>{
          console.log('kaabot ko diri', index)
          if(index>0 && accumulator[counter - 1].CustomerID==currentValue.CustomerID) {
              accumulator[counter - 1].eventGroup.push({
                event: currentValue.claim_status,
                date: currentValue.date_transacted,
                claimtype: currentValue.claim_type,
                representative: currentValue.representative_name,
                tshirt_size: currentValue.tshirt_size,
                transacted_by: currentValue.transacted_by
              })
          } else {
            accumulator[counter] = currentValue
            accumulator[counter].eventGroup = [{
              event: currentValue.claim_status,
              date: currentValue.date_transacted,
              claimtype: currentValue.claim_type,
              representative: currentValue.representative_name,
              tshirt_size: currentValue.tshirt_size,
              transacted_by: currentValue.transacted_by
            }]
            console.log('index here',accumulator)
            counter++
          }
          
          return accumulator
        }
        
        return Object.values(dataList.recordset.reduce(reducer, {}))

    },


    getAvailableTshirtYear: function () {

        //just comment out or remove the unwanted year.
        return [
       // {year: '2021', label: '2021 (MIGS as of Dec. 2020)'},
          {year: '2022', label: '2022 (MIGS as of Dec. 2021)'},
          {year: '2023', label: '2023 (MIGS as of Dec. 2022)'},
        ]
    },

    insertClaim: async function (requestx, claim, year) {
      const db_choice = databases.getDbBranches('tshirt');
      const pool = await getPool(db_choice.database_alias, db_choice.database_name)
      const transaction = new sql.Transaction(pool)

      if(claim.shirtSize=='' || claim.shirtSize==null || claim.shirtSize.length<=0) {
        return {
          status: false,
          message: 'No size selected'
        }
      }

      return transaction.begin().then(async (err)=>{
        try {
          const request = new sql.Request(transaction)
        let updateHeader = await request.query(`UPDATE migs_nonmigs SET claim_status='CLAIMED' where CustomerID='${claim.customerId}' and claim_status<>'CLAIMED' and migs_year=${claim.batchyear}`)
        if(updateHeader.rowsAffected[0]) {
          
          request.input('fk_customer_id', sql.VarChar, claim.customerId)  
          request.input('claim_status', sql.VarChar, 'CLAIMED')
          request.input('transacted_by', sql.VarChar, claim.transacted_by)
          request.input('date_transacted', sql.DateTime, typeof(claim.claim_date)=='undefined' ? new Date() : claim.claim_date)
          request.input('source_ip', sql.VarChar, requestx.headers['x-forwarded-for'] || requestx.socket.remoteAddress)
          request.input('claim_type', sql.VarChar, claim.claim_type)
          request.input('representative_name', sql.VarChar, claim.representative)
          request.input('tshirt_size', sql.VarChar, claim.shirtSize)
          request.input('batch_year', sql.VarChar, claim.batchyear)

          let insertData = await request.query(`INSERT INTO [dbo].[claim_logs]
                  ([fk_customer_id]
                  ,[claim_status]
                  ,[transacted_by]
                  ,[date_transacted]
                  ,[source_ip]
                  ,[claim_type]
                  ,[representative_name]
                  ,[tshirt_size]
                  ,[batch_year])
            VALUES
                  (@fk_customer_id
                  ,@claim_status
                  ,@transacted_by
                  ,@date_transacted
                  ,@source_ip
                  ,@claim_type
                  ,@representative_name
                  ,@tshirt_size
                  ,@batch_year)`)


            await transaction.commit()
            return {
              status: true,
              message: 'succeeded'
            }
          }

        else {
          await transaction.rollback()
          return {
            status: false,
            message: 'no rows affected'
          }
        }
        } catch (error) {
          await transaction.rollback()
          return {            
            status: false,
            message: error.message
          }
        }
      }).catch(err=>{
        return err
      })
    },

    insertReturn: async function (requestx, claim) {
      const db_choice = databases.getDbBranches('tshirt');
      const pool = await getPool(db_choice.database_alias, db_choice.database_name)
      const transaction = new sql.Transaction(pool)

      return transaction.begin().then(async (err)=>{
        try {
          const request = new sql.Request(transaction)
        let updateHeader = await request.query(`UPDATE migs_nonmigs SET claim_status='RETURNED' where CustomerID='${claim.customerId}' and claim_status<>'RETURNED' and migs_year=${claim.batchyear}`)
        if(updateHeader.rowsAffected[0]) {
          
          request.input('fk_customer_id', sql.VarChar, claim.customerId)  
          request.input('claim_status', sql.VarChar, 'RETURNED')
          request.input('transacted_by', sql.VarChar, claim.transacted_by)
          request.input('date_transacted', sql.DateTime, new Date())
          request.input('source_ip', sql.VarChar, requestx.headers['x-forwarded-for'] || requestx.socket.remoteAddress)
          request.input('claim_type', sql.VarChar, claim.claim_type)
          request.input('representative_name', sql.VarChar, claim.representative)
          request.input('tshirt_size', sql.VarChar, claim.shirtSize)
          request.input('batch_year', sql.VarChar, claim.batchyear)

          let insertData = await request.query(`INSERT INTO [dbo].[claim_logs]
                  ([fk_customer_id]
                  ,[claim_status]
                  ,[transacted_by]
                  ,[date_transacted]
                  ,[source_ip]
                  ,[claim_type]
                  ,[representative_name]
                  ,[tshirt_size]
                  ,[batch_year])
            VALUES
                  (@fk_customer_id
                  ,@claim_status
                  ,@transacted_by
                  ,@date_transacted
                  ,@source_ip
                  ,@claim_type
                  ,@representative_name
                  ,@tshirt_size
                  ,@batch_year)`)


            await transaction.commit()
            return {
              status: true,
              message: 'succeeded'
            }
          }

        else {
          await transaction.rollback()
          return {
            status: false,
            message: 'no rows affected'
          }
        }
        } catch (error) {
          await transaction.rollback()
          return {            
            status: false,
            message: error.message
          }
        }
      }).catch(err=>{
        return err
      })
    },



    provideTerminationReasons: function() {
        return [
          'Close - Membership Withdrawal',
          'Close - For Transfer to MAIN Branch',
          'Close - For Transfer to NABUN Branch',
          'Close - For Transfer to CARMEN Branch',
          'Close - For Transfer to BAJADA Branch',
          'Close - For Transfer to MATINA Branch',
          'Close - For Transfer to MINTAL Branch',
          'Close - For Transfer to PANABO Branch',
          'Close - For Transfer to MARKET Branch',
          'Close - For Transfer to STM Branch',
          'Close - For Transfer to DIGOS Branch',
          'Close - For Transfer to SAN FRANS Branch',
          'Close - For Transfer to BUTUAN Branch',
          'Close - For Transfer to MATI Branch',
          'Close - For Transfer to GENSAN Branch',
          'Close - For Transfer to VALENCIA Branch',
          'Close - For Transfer to KORONADAL Branch',
          'Close - For Transfer to MALITA Branch',
          'Close - For Transfer to BAYUGAN Branch',
          'Close - For Transfer to KIDAPAWAN Branch',
          'Close - For Transfer to SURIGAO Branch', //20
          'Close - For Transfer to SAMAL Branch',
          'Close - For Transfer to SURIGAO Branch',
          'Close - For Transfer to GINGOOG Branch',
          'Close - For Transfer to TANDAG Branch',
          'Close - For Transfer to BISLIG Branch',
          'Close - For Transfer to MALAYBALAY Branch',
          'Close - Charge-off', // 27
          'Close - Rejected', // 28
          'Close - Deceased', // 29
          'Close - Terminated', // 30
          'Close - Inactive', // 31
          'Close - Expelled', // 32
          'Close - Voluntary Termination', // 33
          'Close - Double Account', // 34
        ]
    },

    updateUMIDStatus: async function (customerid, branch, umid, statusCode, user, approved_by) {

      // return {
      //   status: 'success',
      //   message: 'ok!'
      // }
      
      try {
        const db_choice = databases.getDbBranches('umid');
      const pool = await getPool(db_choice.database_alias, db_choice.database_name)
      const transaction = new sql.Transaction(pool)

      await transaction.begin()

      const request = new sql.Request(transaction)

      let branchConnected = await request.query(`select top 1 * from _umidactivebranch
                                                  where customer_id='${customerid}' and mem_status='ACTIVE'`)
      
      let mem_status = this.provideTerminationReasons()[statusCode]
      
      if(branchConnected.recordset.length) {
        let updatedRows = await request.query(`UPDATE _umidactivebranch set mem_status='${mem_status}', date_updated='${moment(Date.now()).format('YYYY-MM-DD HH:mm:ss.ss')}',
                                              transaction_type='-', transaction_description='Closed via ICAS Termination Facility'
                                              where customer_id='${customerid}'
                                               and branch_code='${branch}'`)
        //console.log('uprows', updatedRows) has rowsAffected: [ 1 ]
        if(updatedRows.rowsAffected[0]) {
          let insertedRow = await request.query(`INSERT INTO [dbo].[_UMIDBranchTransactions]
                                                        ([fk_umid]
                                                        ,[transaction_date]
                                                        ,[transaction_type]
                                                        ,[requested_by]
                                                        ,[date_requested]
                                                        ,[approved_by]
                                                        ,[date_approved]
                                                        ,[remarks]
                                                        ,[branch_code]
                                                        ,[status])
                                                  VALUES
                                                        ('${umid}'
                                                        ,'${moment(Date.now()).format('YYYY-MM-DD HH:mm:ss.ss')}'
                                                        ,'${mem_status}'
                                                        ,'${user}'
                                                        ,'${moment(Date.now()).format('YYYY-MM-DD HH:mm:ss.ss')}'
                                                        ,'${approved_by}'
                                                        ,'${moment(Date.now()).format('YYYY-MM-DD HH:mm:ss.ss')}'
                                                        ,'Processed thru API'
                                                        ,'${branch}'
                                                        ,'')`)
        }

        transaction.commit()
        return {
          status: 'success',
          message: 'ok!'
        }
      } else {
        transaction.rollback()
        return {
          status: 'invalid',
          message: 'No active accounts found in UMID'
        }
      }
      } catch (error) {
        return {
          status: 'invalid',
          message: error.message
        }
      }

    }
}