
var main_backup =  {
    user: 'User1',
    password: 'Password1',
    server: 'localhost',
    database: 'sample_db',
    options: {
        encrypt: true,
        enableArithAbort: true
    }
};


var nabun_backup =  {
    user: 'sa',
    password: 'sa',
    server: '192.168.161.14',
    database: 'NABUN_08312020',
    options: {
        encrypt: true,
        enableArithAbort: true
    }
};

var carmen_backup =  {
    user: 'sa',
    password: 'sa',
    server: '192.168.161.14',
    database: 'CARMEN_08312020',
    options: {
        encrypt: true,
        enableArithAbort: true
    }
};


module.exports = {
    main: main_backup,
    nabun: nabun_backup,
    carmen: carmen_backup
}