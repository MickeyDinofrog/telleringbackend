var sql = require('mssql');
var databases = require('./NewDatabase');
var {getPool} = require('./ConnectionPools');



module.exports = {

    getCustomers: async function() {

        const pool = await getPool('main', databases.nabun);
            
        return pool.request().query(`select top 10 * from customer`).then(
            (result)=>{
               // console.log('res', result)
                return result;

            }).catch(e=>{
                console.log('errorr is me ', e);    
            });
            
       
    },

    insertAuthor: async function(authorHeader, bookDetails=[]) {

        const pool = await getPool('main', databases.main);
        
        //insert author and his books.
        const transaction = new sql.Transaction(pool);

       return transaction.begin().then( async (resx)=>{

            let has_no_error = true;

            const request = new sql.Request(transaction)

            has_no_error =  await ( request.query(`INSERT INTO [dbo].[author]
                            ([fname]
                            ,[mname]
                            ,[lname])
                            VALUES
                            ('` + authorHeader.name + `'
                            ,'` + authorHeader.mname + `'
                            ,'` + authorHeader.lname + `'); SELECT SCOPE_IDENTITY() AS latest_id`).then(async (result)=>{
                                
                                let loopNoError = true;
                                for(const values of bookDetails)
                                {
                                    
                                    const request_books = new sql.Request(transaction)
                                    
                                 loopNoError = await( request_books.query(`INSERT INTO [dbo].[books]
                                        ([fk_author_id]
                                        ,[book_name]
                                        ,[date_published])
                                        VALUES
                                        (` + result.recordset[0].latest_id + `
                                        ,'` + values.book_name + `'
                                        ,'`+ values.date_published + `')`).then(res=> {
                                                return true;
                                        }).catch(
                                            err=>{
                                                console.log("err", err)
                                                return false;
                                            }
                                        )
                                    
                                    );

                                    if(!loopNoError) break;
                                   
                                }

                                return loopNoError;
                                

                            }).catch(err=>{
                                console.log("errxx", err)
                                return false;

                            })
                );

            if(has_no_error)
            {
                return transaction.commit().then(res=>{
                    return "done insert!";
                })
            }

            else return transaction.rollback().then(res=>{
                return "error on insert.";
            });




        }).catch(err=>{
            return "error";
        })



    },


    updateAuthor: async function(authorObj) {

        const pool = await getPool('main', databases.main);

        const transaction = new sql.Transaction(pool);

        return transaction.begin().then(async (res)=>{
            
            const request = new sql.Request(transaction);
            let isGood = {
                status: true,
                message: '',
            };
           isGood = await(
                            request.query(`UPDATE [dbo].[author]
                            SET [fname] = '` + authorObj.fname +`'
                            ,[mname] = '` + authorObj.mname + `'
                            ,[lname] = '` + authorObj.lname + `'
                        WHERE id = ` + authorObj.id).then(res=>{

                            
                                return {
                                    status: true,
                                    message: Number(res.rowsAffected[0])>=1 ? res.rowsAffected[0] + " row/s updated" : "No data updated"
                                };
                }).catch(err=>{
                    return {
                        status: false,
                        message: err
                    };
                })
           );

           if(isGood.status)
                return transaction.commit().then(res=>{
                    return isGood.message;
                })
        else return transaction.rollback().then(res=>{
            return isGood.message;
        })


        }).catch(err=>{

        })
        

    },






}

//var databases = require('./Database');
//const { getPool } = require('./ConnectionPools')
