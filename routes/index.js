var express = require('express');
var router = express.Router();
var routehandler = require('./route-handler.js');
var GeneralModel = require('../models/GeneralController');
const PDFDocument = require('pdfkit')
const converter = require('number-to-words')
const _ = require('lodash')

function upperCaseEveryWord (str) {
    const splitStr = str.toLowerCase().split(' ')
    for (let i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1)
    }
    // Directly return the joined string
    return splitStr.join(' ')
  }

function  convertToWordsCoopStyle (amount) {
    amount = amount.toString()
    const splitAmount = amount.split('.')
    const firstPart = converter.toWords(splitAmount[0])
    let secondPart = ''
    if (splitAmount.length > 1 && splitAmount[1]!='00') {
      secondPart = ' and ' + splitAmount[1] + '/100'
    }

    return firstPart + secondPart
  }

function getMonthLabel (month) {
  if (month == 0) return 'January'
  else if (month == 1) return 'February'
  else if (month == 2) return 'March'
  else if (month == 3) return 'April'
  else if (month == 4) return 'May'
  else if (month == 5) return 'June'
  else if (month == 6) return 'July'
  else if (month == 7) return 'August'
  else if (month == 8) return 'September'
  else if (month == 9) return 'October'
  else if (month == 10) return 'November'
  else if (month == 11) return 'December'
}


/* GET home page. */
router.get('/xxy', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/harold', routehandler.showdata);

router.get('/', function(req, res) {
  res.send('<b>Congratulations! You have enabled the ICT Endpoint for the web, you may now <span style="color:green;">go back and proceed</span></b>')
})








router.get('/atd', async (req, res)=>{
 try {
  console.log(req.query.maturityDate)
  let loanFormDetails = await GeneralModel.getLoanFormsDetails(req.query.branchcode, req.query.accountnumber)

  let fixedAmort = loanFormDetails.monthlyamort.toFixed(2)
  let amortWords = convertToWordsCoopStyle(fixedAmort)
  let formattedFixedAmort = new Intl.NumberFormat().format(fixedAmort)

  let fixedPrincipal = loanFormDetails.Principal.toFixed(2)
  let principalWords = convertToWordsCoopStyle(fixedPrincipal)
  let formattedFixedPrincipal = new Intl.NumberFormat().format(fixedPrincipal)

  var amortStart = _.cloneDeep(loanFormDetails.DisbursedDate)
  amortStart = new Date(amortStart.setMonth(amortStart.getMonth() + 1))
  var amortEnd = req.query.maturityDate == '' ? loanFormDetails.MaturityDate : new Date(req.query.maturityDate)

  const doc = new PDFDocument({
    size: 'LEGAL'
  })

  doc.pipe(res)
  doc.font('public/fonts/ARIALNB.TTF')
  doc.text(`AUTHORITY TO DEDUCT`, {
    align: 'center'
  })
  doc.fontSize(11);
  doc.text(`THROUGH THE DEPED AUTOMATIC PAYROLL DEDUCTION SYSTEM (APDS)`, {
    align: 'center'
  })
  doc.fillOpacity(0).text('s')
  doc.font('public/fonts/ARIALN.TTF')
  doc.fillOpacity(0).text('aaaaaaa', {
    continued: true,
    align: 'justify'
  }).fillOpacity(1).text(`I hereby authorize DepEd to deduct monthly from my salary, through the DepEd APDS, the sum of `, {
    continued: true,
  }).font('public/fonts/ARIALNB.TTF').text(`PESOS: ${upperCaseEveryWord(amortWords)} (P${formattedFixedAmort}), `, {
    continued: true,
    underline: true
  }).font('public/fonts/ARIALN.TTF')
  .text(` inclusive of principal and interest,beginning on `, {
    underline: false,
    continued: true
  }).font('public/fonts/ARIALNB.TTF').text(`${getMonthLabel(amortStart.getMonth())} ${amortStart.getFullYear()} `, {
    underline: true,
    continued: true
  }).font('public/fonts/ARIALN.TTF').text(`and ending on `, {
    underline: false,
    continued: true
  }).font('public/fonts/ARIALNB.TTF').text(`${getMonthLabel(amortEnd.getMonth())} ${amortEnd.getFullYear()}, `, {
    underline: true,
    continued: true
  }).font('public/fonts/ARIALN.TTF').text(`and to remit the same to `, {
    underline: false,
    continued: true
  }).font('public/fonts/ARIALNB.TTF').text(`Tagum Cooperative `, {
    underline: true,
    continued: true
  }).font('public/fonts/ARIALN.TTF').text(`in consideration of the loan which was granted to me on `, {
    underline: false,
    continued: true
  }).font('public/fonts/ARIALNB.TTF').text(`${getMonthLabel(loanFormDetails.DisbursedDate.getMonth())} ${loanFormDetails.DisbursedDate.getDate()}, ${loanFormDetails.DisbursedDate.getFullYear()}.`, {
    underline: true
  })

  doc.fillOpacity(0).text('s')
  doc.font('public/fonts/ARIALN.TTF')
  doc.fillOpacity(0).text('aaaaaaa', {
    continued: true,
    align: 'justify'
  }).fillOpacity(1).text(`The authorization is VALID and BINDING within the aforementioned loan period, unless the loan is pre-terminated, or the authorization is otherwise revoked. Moreover, I agree that deductions that will reduce my monthly net take-home pay to lower than what is allowed under the law shall not be accommodated in the APDS. Such non-accommodation shall not extend the ending period of this authorization.`)
  doc.fillOpacity(0).text('s')
  doc.fillOpacity(1).text(`${loanFormDetails.fullname}`)
  doc.fillOpacity(1).font('public/fonts/ARIALNB.TTF').text('Signature over Printed Name of DepEd Borrower')
  doc.fillOpacity(0).text('s')
  doc.fillOpacity(1).text('Date: ', {
    continued: true,
  }).text('                                              ', {
    underline: true
  })

  doc.lineWidth(3)
  doc.moveTo(doc.x, 310)
   .lineTo(doc.page.width - 70, 310).stroke()
   doc.fillOpacity(0).text('s')
   doc.text('s')
   doc.text('s')
  doc.fillOpacity(1).text(`PROMISSORY NOTE`, {
    align: 'center'
  })
  doc.fillOpacity(0).text('s')
  doc.text('aaaaaa', {
    align: 'justify',
    continued: true
  }).fillOpacity(1).font('public/fonts/ARIALN.TTF').text(`For value received, the undersigned promises to pay through APDS to the `, {
    continued: true
  }).font('public/fonts/ARIALNB.TTF').text(`Tagum Cooperative `, {
    continued: true,
    underline: true
  }).font('public/fonts/ARIALN.TTF').text(`the sum of `, {
    continued: true,
    underline: false
  }).font('public/fonts/ARIALNB.TTF').text(`PESOS: ${upperCaseEveryWord(principalWords)} Pesos (P${formattedFixedPrincipal}) `, {
    continued: true,
    underline: true
  }).font('public/fonts/ARIALN.TTF').text(`with interest rate of seven point five percent (7.5%) per annum, `, {
    continued: true,
    underline: false
  }).font('public/fonts/ARIALNB.TTF').text(`TO BE PAID IN EQUAL MONTHLY INSTALLMENTS, INCLUSIVE OF PRINCIPAL AND INTEREST, IN THE AMOUNT OF P${formattedFixedAmort} BEGINNING ON ${getMonthLabel(amortStart.getMonth())} ${amortStart.getFullYear()} AND ENDING ON ${getMonthLabel(amortEnd.getMonth())} ${amortEnd.getFullYear()}.`)

  doc.fillOpacity(0).text('s')
  doc.text('aaaaaa', {
    align: 'justify',
    continued: true
  }).fillOpacity(1).font('public/fonts/ARIALN.TTF').text(`Default in the payment for six (6) consecutive installments shall render the entire unpaid balance due and demandable.`)
  doc.fillOpacity(0).text('s')
  doc.fillOpacity(1).text(`IN WITNESS WHEREOF, I have hereunto set my hand this ________day of __________ at _______________.`)
  doc.fillOpacity(0).text('s')
  doc.text('s')
  doc.fillOpacity(1).text(`${loanFormDetails.fullname}`, {
    underline: true
  })
  doc.font('public/fonts/ARIALNB.TTF').text(`(Signature over Printed Name of Borrower)`)
  doc.fillOpacity(0).text('s')
  doc.text('s')
  doc.fillOpacity(1).font('public/fonts/ARIALN.TTF').text(`Employee No.______Division No.____Station No._____           Date Issued___________________`)
  doc.text('School Address:                                                                          Place Issued__________________')
  doc.text('______________________________________________  Telephone Number: ________________')
  doc.fillOpacity(0).text('s')
  doc.fillOpacity(1).text(`Home Address: ______________________________________________`)
  doc.fillOpacity(0).text('s')
  doc.fillOpacity(1).text(`       Subscribed and sworn to before me, this ________ day of ___________ 20____, the affiant identified as such person after presenting the following:`)
  doc.fillOpacity(0).text('s')
  doc.font('public/fonts/ARIALNB.TTF').fillOpacity(1).text(`       NAME                                                        ID NO.                                             DATE AND PLACE ISSUED`)
  doc.fillOpacity(0).text('s')
  doc.text('s')
  doc.fillOpacity(1).text(`NOTARY PUBLIC                           .`, {
    align: 'right'
  })
  doc.font('public/fonts/ARIALN.TTF').fillOpacity(1).text('Doc. No______________')
  doc.text('Page No______________')
  doc.text('Book No______________')
  doc.text('Series of______________')
  doc.end()

 } catch (error) {
   res.status(500).send('something went wrong: ' + error.message)
 }

})


module.exports = router;
