var express = require('express');
var router = express.Router();
var ShortageOverageReport = require('../models/ShortageOverageReport')
var SystemUser = require('../models/SystemUser');
var TellerRunningDate = require('../models/TellerRunningDate')
var ObservationReport = require('../models/ObservationReport')
var Login = require('../models/Login');
const BasiAuth = require('../models/BasiAuth');
const basicAuth = require('express-basic-auth')

/* GET users listing. */
router.get('/harold', function(req, res) {

  ShortageOverageReport.findById('eballo').then((response)=>{
    console.log(response);
    res.send(response.recordset[0].FirstName);
  }
  );
 
  
});


router.get('/get-users', function(req, res){

SystemUser.findAllByBranchToFillGrid('015').then((response)=>{
  res.json(response);
});
});

router.post('/change-password', function(req, res) {
  Login.changePassword(req.body).then(resx=>{
    res.status(200).send(resx)
  })
})

router.get('/save-date', function(req,res){
  
  var dates = [
    '3/18/2020',
    '3/19/2020',
    '3/20/2020',
    '3/21/2020',
    '3/22/2020'
  ];

  TellerRunningDate.save(dates).then((response)=>{
      res.send("watta");
  });
})


router.get('/find-all-dates/:date_string', function(req, res) {
  TellerRunningDate.findAll(req.params.date_string).then((response)=>{
    res.json(response.recordset);
  })
})

router.get('/find-one-date/:id', function(req, res) {
  TellerRunningDate.findOneDate(req.params.id).then((response)=>{
    res.json(response.recordset);
  })
})


router.get('/find-all-tellers', function(req, res) {
  console.log("branch code is " + req.query.transaction_date)
  SystemUser.findAllActiveTellers(req.query.branchcode).then((response)=>{
    res.json(response);
  }).catch(e=>{
    res.status(404).send(error)
  })
  })


  router.get('/find-all-tellers-transaction/:branchcode/:date_id', function(req, res) {
 
    SystemUser.findAllActiveTellersTransaction(req.params.branchcode, req.params.date_id).then((response)=>{
      res.json(response.recordset);
    })
    })


  router.post('/save-shortage-overage-report', (req, res)=>{

   //  let wtf = JSON.parse(req.fields.details)
   // console.log(wtf.to_insert);
    ShortageOverageReport.save(req.body.to_insert, req.body.date_id, req.body.created_by).then((response)=>{
      res.json(response);
    })
  })

  router.post('/declare-teller', (req, res)=>{
    ShortageOverageReport.saveDeclaration(req.body.to_insert, req.body.userObj).then(response=>{
      res.json(response)
    })
  })

router.get('/get-transaction-type', (req, res)=>{

})

router.get('/get-observation-index/:branchcode', (req, res)=>{
  try {
    ObservationReport.getObservationIndex(req.params.branchcode).then(result=>{
     
     res.status(200).send(result.recordset)
    });
  } catch (error) {
    res.status(404).send(error)
  }
})


router.get('/get-shortageoverage-index/:branchcode', (req, res)=>{
  try {

    ShortageOverageReport.getShortageOverageIndex(req.params.branchcode).then(result=>{
     
     res.status(200).send(result.recordset)
    });
  } catch (error) {
    res.status(404).send(error)
  }
})


router.post('/save-observation-report', (req, res)=>{
  try {

    ObservationReport.save(req.body.observation_report).then(resx=>{
        if(resx.status=='succeeded')
        res.status(200).send('succeeded')
        else res.status(404).send(resx.message)
    });
    
  } catch (error) {
   
    res.status(404).send(error)
  }
})


router.get('/initialize-observation-report', (req, res)=>{
  var retval = {};
  SystemUser.findAllActiveTellersForDropdown(req.query.branch_code, req.query.date_id).then(response=>{
    retval.all_available_tellers = response;
    console.log('dawbii', retval);
    TellerRunningDate.findAll(response[0].currenttransactiondate, false).then((responsex)=>{
      console.log('diri kaabot ko bai')
      retval.all_tran_dates = responsex.recordset;

      ObservationReport.findAllTransactionType().then((responsez)=>{
        retval.all_tran_type = responsez;
        
        res.send(retval);
      });

    }).catch((err)=>{
      console.log(response[0].currenttransactiondate);
      res.send("woww " + err.message)
    })

    //retval.all_available_tellers = response.recordset
   
  })

  

})

router.get('/get-observation-data-for-update/:date_id/:branch_id',(req, res)=>{
    ObservationReport.getAllDataForUpdate(req.params.date_id, req.params.branch_id).then(response=>{
      res.status(200).send(response.recordset)
    })
})


router.get('/get-all-tellers-for-default',(req, res)=>{


    let date_object = JSON.parse(req.query.tran_date)
  ObservationReport.getAllTellersForDefault(date_object.value, date_object.label, req.query.branch_code, req.query.user).then(response=>{
    res.status(200).send(response)
  })
})


router.post('/update-observation', (req, res)=>{
  try {

    ObservationReport.updateObservation(req.body.observation_report, req.body.methodx).then(resx=>{
      res.status(200).json(resx)
    });
    
  } catch (error) {
   
    res.status(404).send(error)
  }
})


router.post('/update-shortage-overage', (req, res)=>{
  try {

    ShortageOverageReport.updateShortageOverage(req.body.observation_report, req.body.methodx).then(resx=>{
      res.status(200).json(resx)
    });
    
  } catch (error) {
   
    res.status(404).send(error)
  }
})

router.get('/initialize-coci', (req, res)=>{
 try {
   
  SystemUser.initializeCoci(req.query.branch_code).then(resx=>{
    console.log('bend my will again', resx);
    res.status(200).send(resx);
  })

   
 } catch (error) {
   res.status(404).send(error);
 }

  
})



router.get('/initialize-coci-index', (req, res)=>{
  try {
    

   SystemUser.initializeCociIndex(req.query.selectiontype, req.query.branchcode, req.query.currentuser).then(resx=>{
     console.log('yow abot ko here');
     res.status(200).send(resx);
   })
 
    
  } catch (error) {
    res.status(404).send(error);
  }

 })


 router.get('/load-teller-branches', (req, res)=>{
  try {
    

   SystemUser.loadTellerBranches().then(resx=>{
    
     res.status(200).send(resx);
   })
 
    
  } catch (error) {
    res.status(404).send(error);
  }

 })


 router.get('/get-id-of-date', (req, res)=>{
  try {
    

   SystemUser.getIdOfTelleringDateString(req.query.date_string).then(resx=>{
 
     res.status(200).send(resx);
   })
 
    
  } catch (error) {
    res.status(404).send(error);
  }

 })


 router.get('/get-id-of-coci-header', (req, res)=>{
  try {
    

   SystemUser.getIdOfCociHeader(req.query.username, req.query.date_string).then(resx=>{
 
     res.status(200).send(resx);
   })
 
    
  } catch (error) {
    res.status(404).send(error);
  }

 })


 router.get('/get-all-tellers-and-branches', (req, res)=>{
  try {
    

   SystemUser.getAllTellersAndBranches().then(resx=>{
 
     res.status(200).send(resx);
   })
 
    
  } catch (error) {
    res.status(404).send(error);
  }

 })


 router.get('/load-coci-details', (req, res)=>{
  try {
    

   SystemUser.loadCociDetails(req.query.coci_id).then(resx=>{
     console.log('yow abot ko here');
     res.status(200).send(resx);
   })
 
    
  } catch (error) {
    res.status(404).send(error);
  }

 })



router.post('/save-coci-report', (req, res)=>{
  try {

    console.log('dayum', req.body.payload_object);

    SystemUser.saveCOCIReport(req.body.payload_object).then(resx=>{
      res.status(200).json(resx);
    });


    
  } catch (error) {
   
    res.status(404).send(error)
  }
})


/** for tshirt */

router.post('/login', (req, res)=>{
  Login.loginUser(req.body.branch, req.body.username, req.body.password).then(resx=>{
    res.status(200).send(resx);
  })
});

router.get('/get-clients', (req, res)=>{
  Login.getMigsSearch(req.query.branchcode, req.query.name, req.query.year).then(resx=>{
    res.status(200).send(resx)
  })
})

router.post('/save-claim', (req, res)=>{
  Login.insertClaim(req, req.body).then(resx=>{
    res.status(200).send(resx)
  })
})

router.post('/save-return', (req, res)=>{
  Login.insertReturn(req, req.body).then(resx=>{
    res.status(200).send(resx)
  })
})

router.get('/get-available-years', (req, res)=>{
  let resx = Login.getAvailableTshirtYear();
  res.status(200).send(resx);
})

router.post('/close-account', basicAuth({users: {'jesrel': 'dumanacalx'}}), async (req, res)=>{

  console.log(req.body)

  let response = await Login.updateUMIDStatus(req.body.customerid, req.body.branch, req.body.umid, req.body.statusCode, req.body.user, req.body.approved_by)
  console.log('response is', response)
   res.status(200).send(response)
})
module.exports = router;
