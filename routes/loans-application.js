var express = require('express');
var router = express.Router();
var SystemUser = require('../models/SystemUser');

const formidable = require('formidable');
const fs = require('fs');
path = require('path');


/* GET users listing. */
router.get('/get-all-pending', function(req, res) {
    
     SystemUser.findAllPendingLoans(req.query.branchcode).then((result)=>{
            res.json(result)
     })   
    });

 router.get('/test-pooling', function(req, res) {
      
       SystemUser.findAllByBranchToFillGrid('015').then((result)=>{
              res.json(result)
       })   
      });

router.post('/save-request', (req, res)=>{
  try {
    console.log("ha?", req.body)
    SystemUser.saveForApproval(req.body.to_insert, req.body.files_upload, req.body.id_files_upload).then(result=>{
      console.log("ako ang pinakalast nga resort naunsa", result)
      res.json(result)
    }).catch(err=>{console.log(err)})
  } catch (error) {
    console.log("error ka bro", error)
  }
})

router.post('/approve-request', (req, res)=>{
  try {

    SystemUser.approveRequest(req.body).then(result=>{
      console.log("naupdate na powhz")
      res.json(result)
    });
    
  } catch (error) {
    console.log("nag error imong update", error)
  }
})


router.get('/get-voucher-files/:fk_request_id', (req, res)=>{
  console.log("req id is " + req.params.fk_request_id)
  var retval = {};
  SystemUser.findAttachments(req.params.fk_request_id).then((response)=>{
    retval.uploaded_files = response.recordset;

      SystemUser.findRespectiveApprovers(req.params.fk_request_id).then((response)=>{
         retval.evaluators = response.recordset;
         
            SystemUser.findHeaderById(req.params.fk_request_id).then((response)=>{
              retval.header = response.recordset;
              res.json(retval);
            })
      })

    

  })

  SystemUser.findHeaderById(req.params.fk_request_id).then((response)=>{
    retval.header = response.recordset;
  })
})

router.get('/get-individual-files', (req, res)=>{

  var options = {
    root: path.join(__dirname, 'files'),
  }
  res.sendFile(req.query.filename, options, (err)=>{
    if(err) console.log("error send file", err)
    else console.log('sent: ', req.query.filename)
  })
})

router.post('/update-status', (req, res)=>{
  try {
    SystemUser.updateRequestStatus(req.body).then(response=>{
      res.json(response)
  });
  } catch (error) {
    console.log("hoy imong update status nag-error", error)
  }
});

router.post('/update-request', (req, res)=>{
  try {
    console.log("ha?", req.body)
    SystemUser.updateFilesUpload(req.body.to_insert, req.body.files_upload, req.body.id_files_upload).then(result=>{
      res.json(result)
    }).catch(err=>{console.log(err)})
  } catch (error) {
    console.log("error ka bro", error)
  }
})

router.post('/delete-file', (req, res)=>{
  try {

    SystemUser.removeFiles(req.body).then((result)=>{
      let root = path.join(__dirname, 'files/'+req.body.file_name);
      fs.unlinkSync(root);
      res.json(result)
    })

    
    //console.log("successfully deleted")
  } catch (error) {
    console.log("nage error imong delete haha", error)
  }
})


module.exports = router;