var express = require('express');
var router = express.Router();
var LoanComputation = require('../models/LoanComputations');


router.get('/try-me', function(req, res) {
  
  try {
    LoanComputation.computeESPLoan(req.query.customerid).then(
      (result)=>{
        res.json(result);
      })
  } catch (error) {
    res.statusCode(500).send(error);
  }
  
});



module.exports = router;