var express = require('express');
var router = express.Router();

const fs = require('fs');
//var rawdata = fs.readFileSync('./query/queries.json');
//var queries = JSON.parse(rawdata);
var databases = require('../models/Database');

var routes = function() {
	//getallcustomerbyid
	router.get('/get-customers/:id', async function(req, res, next) {
		try {
			if (req.params.id != null) {
			
				const db_choice = databases.getDbBranches('jel'); 
				const pool = await getPool(db_choice.database_alias, db_choice.database_name)
                

				const result = await pool.request().input('id', sql.VarChar, req.params.id).query(queries.getcustomer);
				res.json(result);
			} else {
				res.send('No data');
			}
		} catch (error) {
			res.status(500);
			res.send(error.message);
		}
	});
	//getallcustomersavings
	router.get('/getCustomerSavings/:id', async function(req, res, next) {
		try {
			if (req.params.id != null) {
				const pool = await connect;
				const result = await pool
					.request()
					.input('id', sql.VarChar, req.params.id)
					.query(queries.getCustomerSavings);
				res.json(result);
			} else {
				res.send('No data');
			}
		} catch (error) {
			res.status(500);
			res.send(error.message);
		}
	});
	//getallcustomershares
	router.get('/getCustomerShares/:id', async function(req, res, next) {
		try {
			if (req.params.id != null) {
				const pool = await connect;
				const result = await pool
					.request()
					.input('id', sql.VarChar, req.params.id)
					.query(queries.getCustomerShares);
				res.json(result);
			} else {
				res.send('No data');
			}
		} catch (error) {
			res.status(500);
			res.send(error.message);
		}
	});
	//getallcustomershares
	router.get('/getCustomerLoans/:id', async function(req, res, next) {
		try {
			if (req.params.id != null) {
				const pool = await connect;
				const result = await pool
					.request()
					.input('id', sql.VarChar, req.params.id)
					.query(queries.getCustomerLoans);
				res.json(result);
			} else {
				res.send('No data');
			}
		} catch (error) {
			res.status(500);
			res.send(error.message);
		}
	});
	//getallcustomerbyname
	router.get('/getCustomersname/:customername', async function(req, res, next) {
		try {
			if (req.params.customername != null) {
				const pool = await connect;
				const result = await pool
					.request()
					.input('customername', sql.VarChar, req.params.customername)
					.query(queries.getAllcustomer);
				res.json(result);
			} else {
				res.send('No data');
			}
		} catch (error) {
			res.status(500);
			res.send(error.message);
		}
	});

	//getallcustomeraccount_info_savings
	router.get('/getCustomerSavingsDetail/:id', async function(req, res, next) {
		try {
			if (req.params.id != null) {
				const pool = await connect;
				const result = await pool
					.request()
					.input('id', sql.VarChar, req.params.id)
					.query(queries.getCustomerSavingsDetails);
				res.json(result);
			} else {
				res.send('No data');
			}
		} catch (error) {
			res.status(500);
			res.send(error.message);
		}
	});
	//getallcheckITem
	router.get('/getCheckItem', async function(req, res, next) {
		try {
			const pool = await connect;
			const result = await pool.request().query(queries.getallCheckItem);
			res.json(result.recordset);
		} catch (error) {
			res.status(500);
			res.send(error.message);
		}
	});
	// router.get('/getCustomerSavings', async function(req, res, next) {
	// 	try {
	// 		const pool = await connect;
	// 		const result = await pool.request().query(queries.getCustomerSavings);
	// 		res.json(result.recordset);
	// 	} catch (error) {
	// 		res.status(500);
	// 		res.send(error.message);
	// 	}
	// });
	//update useraccount
	// router.post('/saveleave', async function(req, res, next) {
	// 	try {
	// 		if (req.body.reason != null) {
	// 			const pool = await connect;
	// 			const result = await pool
	// 				.request()
	// 				.input('fkleavetype', sql.Int, req.body.leavetype)
	// 				.input('startdate', sql.DateTime, req.body.startdate)
	// 				.input('enddate', sql.DateTime, req.body.enddate)
	// 				.input('reason', sql.VarChar, req.body.reason)
	// 				.query(queries.saveleave);
	// 			res.status(200).send('SAVED');
	// 		} else {
	// 			res.send('No data to update!');
	// 		}
	// 	} catch (error) {
	// 		res.status(500);
	// 		res.send(error.message);
	// 	}
	// });

	// //getIncomeExpense
	// router.get('/getExpenseIncome', async function(req, res, next) {
	// 	try {
	// 		const pool = await connect;
	// 		const result = await pool.request().query(queries.getincomeexpense);
	// 		res.json(result.recordset);
	// 	} catch (error) {
	// 		res.status(500);
	// 		res.send(error.message);
	// 	}
	// });

	// //saveexpense
	// router.post('/saveExpenseIncome', async function(req, res, next) {
	// 	try {
	// 		if (req.body.text != null) {
	// 			const pool = await connect;
	// 			const result = await pool
	// 				.request()
	// 				.input('text', sql.VarChar(50), req.body.text)
	// 				.input('amount', sql.Float, req.body.amount)
	// 				.query(queries.saveexpenseandincome);
	// 			res.status(200).send('SAVED');
	// 		} else {
	// 			res.send('No data to update!');
	// 		}
	// 	} catch (error) {
	// 		res.status(500);
	// 		res.send(error.message);
	// 	}
	// });

	// //deleteTransaction
	// router.delete('/deleteExpenseIncome/:id', async function(req, res, next) {
	// 	try {
	// 		if (req.params.id != null) {
	// 			const pool = await connect;
	// 			const result = await pool
	// 				.request()
	// 				.input('id',sql.Int , req.params.id)
	// 				.query(queries.deleteExpenseandincome);
	// 			res.status(200).send('DELETE');
	// 		} else {
	// 			res.send('No data to delete!');
	// 		}
	// 	} catch (error) {
	// 		res.status(500);
	// 		res.send(error.message);
	// 	}
	// });
	return router;
};
module.exports = routes;
