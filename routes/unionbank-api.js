var express = require('express');
var router = express.Router();
const request = require('request');



/* GET users listing. */
router.get('/serve-token', function(req, res) {

    console.log("hi i am the token", req);

   // res.send("gikan pa kog tagum coop. haha");

    //access token will be processed here

    //check if there is code given
    if(typeof req.query.code != 'undefined'){

        //build the request
        let options = {
            "url" : 'https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/token', //set the url
            headers: {  //shit the headers
                'content-type' : '	application/x-www-form-urlencoded',
                'accept': 'text/html'
            },
            method: 'POST', //set request to POST
            form: { //create the form body of the request
                client_id: '830c9efe-abf1-4f70-a1ad-de192bccf887',          //client ID of Application
                code: req.query.code,                       //put the code we receive from UnionBank Online Login
                redirect_uri: 'https://ubpredirect.loca.lt/unionbank-api/serve-token',    //this must be same with the redirect URI we used initially
                grant_type: 'authorization_code'            //set the grant type to authorization_code
            }
        };
        request(options, function(err, response, body){
            if(!err){
                console.log('Error', err);
            }
            if(response){   //output the result of request
                try{
                    res.json(JSON.parse(body)); //parse the body into json
                }catch(e){
                    res.json(e);    //if error in parsing
                }
            }else{
                res.send(); //send nothing if nothing is processed
            }
        });
    }
   
    
  });


  module.exports = router;