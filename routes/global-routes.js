var express = require('express');
var router = express.Router();
var GeneralController = require('../models/GeneralController');

const formidable = require('formidable');
const fs = require('fs');
path = require('path');

router.get('/getloanlist/:UserName', (req, res)=>{
  try {
      GeneralController.findLoan(req.params.UserName).then(result=>{
        res.json(result.recordset)
      }).catch(err=>{console.log(err)})
  } catch (error) {
    res.status(500)
    res.send(error.message)
  }
})


router.get('/getapproverlist/:FKLoanRole', (req, res)=>{
  try {
      GeneralController.findApproveList(req.params.FKLoanRole).then(result=>{
        res.json(result.recordset)
      }).catch(err=>{console.log(err)})
  } catch (error) {
    res.status(500)
    res.send(error.message)
  }
})

router.get('/getPendingCount/:UserName', (req, res)=>{
  try {
      GeneralController.pendingRequestCount(req.params.UserName).then(result=>{
        res.json(result.recordset)
      }).catch(err=>{console.log(err)})
  } catch (error) {
    res.status(500)
    res.send(error.message)
  }
})

router.get('/getPendingCountApprover/:FKLoanRole', (req, res)=>{
  try {
      GeneralController.pendingCountApprover(req.params.FKLoanRole).then(result=>{
        res.json(result.recordset)
      }).catch(err=>{console.log(err)})
  } catch (error) {
    res.status(500)
    res.send(error.message)
  }
})

router.post('/save-loan-request', (req, res)=>{
  try {
    GeneralController.insertHeader(req.body.to_insert,req.body.username, req.body.files_upload).then(result=>{
      
      res.json({latest_id: result, status: 'Success'})

    }).catch(err=>{console.log(err)})
  } catch (error) {
    console.log("error ka bro", error)
  }
})


router.post('/upload-files', (req, res)=>{
   try {
    const form = new formidable.IncomingForm()
    form.multiples = true;
    const folder = path.join(__dirname, 'files')
    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder)
    }
    form.on('fileBegin', function (name, file){
      var res = file.type.split("/");
      file.path = folder + '/' + file.name+'.'+res[1]
    })
    form.parse(req, (errx, fields, files) => {
      if(errx) {
        console.log("error formidable: ", errx)
      }
      console.log('\n-----------')
      console.log('Fields: ', fields)
      console.log('Received: ', files)
    })
   /* form.on('fileBegin', function (name, file, fields){
      file.path = folder + '/' + file.name
    })*/

    res.json({status: 'Success'})
   } catch (error) {
     console.log(error)
   }

})
  
module.exports = router;
