var express = require('express');
var router = express.Router();
var Login = require('../models/Login');

const jwt = require('jsonwebtoken')
const formidable = require('formidable');
const fs = require('fs');
const accessTokenSecret = 'youraccesstokensecret';



path = require('path');


router.post('/loginaccount', (req, res)=>{
  try {
    if(req.body.UserName != '' && req.body.Password != '') {
      Login.findToken(req.body.UserName).then(result=>{
        if (result.recordset.length > 0) {
          var dateToday = new Date();
          if (dateToday > result.recordset[0].date_expire) {
            const accessToken = jwt.sign({ UserName: result.recordset[0].UserName}, accessTokenSecret)
            Login.updateToken(result.recordset[0].username, accessToken)
              Login.findUser(req.body.UserName, req.body.Password).then(result=>{
                res.json({userdetails: result.recordset, status: 'SAVED', message: '1', accessToken: accessToken})
              }).catch(err=>{console.log(err)})
          }
          else {
            Login.findUser(req.body.UserName, req.body.Password).then(result=>{
            if (result.recordset.length > 0) {
            res.json({userdetails: result.recordset, status: 'SAVED', message: '2', accessToken: result.recordset[0].api_token})
            }
            else res.json({status:'failed', message: 'Invalid Credentials.'});
            }).catch(err=>{console.log(err)})
          }
        }
        else {
            Login.findUser(req.body.UserName, req.body.Password).then(result=>{
            if (result.recordset.length > 0) {
            const accessToken = jwt.sign({ UserName: result.recordset[0].UserName, FKRoleID: result.recordset[0].FKRoleID }, accessTokenSecret)
            Login.insertToken(result.recordset[0].UserName, accessToken)
            res.json({userdetails: result.recordset, status: 'SAVED', message: '3', accessToken: accessToken})
            }
            else res.json({status:'failed', message: 'Invalid Credentials.'});
            }).catch(err=>{console.log(err)})   
        }
      }).catch(err=>{console.log(err)})
    }
  } catch (error) {
    res.status(500).send(error.message)
  }
})

module.exports = router;
